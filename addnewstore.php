<?php
include 'db.php';
$con = new Connection();
include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$salespersonid	= $_POST['userid'];
$store_name		= fnEncodeString($_POST['store_name']);
$owner_name		= fnEncodeString($_POST['owner_name']);
$contact_no		= $_POST['contact_no'];
$store_Address	= fnEncodeString($_POST['store_Address']);
$lat			= $_POST['lat'];
$lng			= $_POST['lon'];
$city			= $_POST['city'];
$state			= $_POST['state'];

$distributorid	= $_POST['distributorid'];
$suburbid		= $_POST['suburbid'];
$subarea_id		= $_POST['subareaid'];

$jsonOutput 	= $JSONVar->fnAddNewStore($salespersonid,$store_name,$owner_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$distributorid,$suburbid,$subarea_id);
echo $jsonOutput;
