<?php
include ("../../includes/config.php");
include ("../includes/common.php");
if(!isset($_SESSION[SESSION_PREFIX."user_id"])) {	
	echo '<script>location.href="../login.php";</script>';
	exit;
}
$condn="";
$condnsearch="";
$selperiod=$_REQUEST['selTest'];
switch($selperiod){
	case 1:
		$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
	break;
	case 2:
		$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
	break;
	case 3:
		if($_REQUEST['todate']!="")
			$todat=$_REQUEST['todate'];
		else
			$todat = date("d-m-Y");
	
		$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$_REQUEST['frmdate']."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
	break;
	case 4:
		$condnsearch=" AND DATE_FORMAT(order_date,'%Y-%m-%d')=DATE(NOW())";
	break;
	case 0:
		$condnsearch="";
	break;
	default:
		$condnsearch="";
	break;
}
switch($_SESSION[SESSION_PREFIX.'user_type']) {
	case "Admin":
		$condn="";
	break;
	case "Superstockist":
		$condn=" AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
	break;
	case "Distributor":
		$condn=" AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
	break;
}
if(isset($_SESSION[SESSION_PREFIX."firstname"]))
{
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<style>
g text:last-child {font-size :15px;}
</style>
<meta charset="utf-8"/>
<title><?=SITETITLE;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css"></script>
<!-- BEGIN THEME STYLES -->
<link href="../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="../../assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<!-- amCharts javascript sources -->
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
		<script type="text/javascript" src="../includes/js/common.js"></script>
		<?php
		if (strpos($_SERVER['SCRIPT_NAME'], 'index.php') !== false){
						$sqlbrands="SELECT oa.categorynm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale' OR vo.campaign_sale_type LIKE '')) WHERE oa.id!=0 ".$condn." ".$condnsearch." GROUP BY oa.categorynm ORDER BY oa.id DESC limit 5";
					$resultbrands = mysqli_query($con,$sqlbrands);
$rows = array();
while ($rowbrands = mysqli_fetch_array ($resultbrands)){
     $rows[] = array(
    "category" => $rowbrands['Name'],
    "column-1" => $rowbrands['Total_Sales']
  );
  }
  $catarr= json_encode( $rows );
   $sqlsuperstock="SELECT oa.superstockistnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale' OR vo.campaign_sale_type LIKE '')) WHERE oa.id!=0 ".$condn." ".$condnsearch." GROUP BY oa.superstockistnm ORDER BY oa.id DESC limit 5";
//exit;
$resultsuperstock = mysqli_query($con,$sqlsuperstock);
$rowssuper = array();
while ($rowsuper = mysqli_fetch_array ($resultsuperstock)){
     $rowssuper[] = array(
    "category" => $rowsuper['Name'],
    "column-1" => $rowsuper['Total_Sales']
  );
  }
  $superstockistarr= json_encode( $rowssuper );

if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor"){
  //stockist wise
  $sqlstock="SELECT oa.salespfullnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale' OR vo.campaign_sale_type LIKE '')) WHERE oa.id!=0 ".$condn." ".$condnsearch." GROUP BY oa.salespersonnm ORDER BY oa.id DESC limit 5";
$resultstock = mysqli_query($con,$sqlstock);
$rowsstock = array();
while ($rowst = mysqli_fetch_array ($resultstock)){
     $rowsstock[] = array(
    "category" => $rowst['Name'],
    "column-1" => $rowst['Total_Sales']
  );
  }
  $stockistarr= json_encode($rowsstock);
  $txtnm="SalesPerson Wise Sales Order";
}
else
{
	//stockist wise
  $sqlstock="SELECT oa.distributornm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale' OR vo.campaign_sale_type LIKE '')) WHERE oa.id!=0 ".$condn." ".$condnsearch." GROUP BY oa.distributornm ORDER BY oa.id DESC limit 5";
$resultstock = mysqli_query($con,$sqlstock);
$rowsstock = array();
while ($rowst = mysqli_fetch_array ($resultstock)){
     $rowsstock[] = array(
    "category" => $rowst['Name'],
    "column-1" => $rowst['Total_Sales']
  );
  }
  $txtnm="Stockist Wise Sales Order";
  $stockistarr= json_encode($rowsstock);
}
?>
			<!-- amCharts javascript code -->
		<script type="text/javascript">
			var chart = AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Superstockist Wise Orders",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": ""
						}
					],
					"dataProvider": <?php echo $superstockistarr;?>
				}
			);			
		</script>		
		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv1",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"fillColors": "#12D812",
							"id": "AmGraph-1",
							"title": "Farmer Wise Sales in ₹",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
					//"dataProvider": <?php //echo $stockistarr;?>
					"dataProvider": <?php echo $superstockistarr;?>
				}
			);
		</script>
		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv2",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "<?php echo $txtnm;?>",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
					"dataProvider": <?php echo $stockistarr;?>
				}
			);
		</script>
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv3",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Category Wise Sales Order",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹" 
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
						"dataProvider":<?php echo $catarr;?>
				}
			);
		</script>		
		<style>
		.fa.fa-inr.fa-6 {font-size: 30px;}
		.amcharts-chart-div > a {display: none !important;}
		</style>
		<?php 
		}// index page...
		?>
</head>
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="./index.php">
			<img src="<?=PORTALLOGOPATH;?>" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
			
		</div>
		
		<div class="page-logo">
		
			<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
		
		</div>
		
		
		
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

					<span class="username ">
					<?php echo "Welcome "."<b>".$_SESSION[SESSION_PREFIX.'firstname']."</b>";?> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
					 
                           <li>
							<a href="profile.php">
							<i class="icon-user"></i> My Profile </a>
						</li>
						  <li>
							<a href="changepassword.php">
							<i class="icon-lock"></i>Change Password </a>
						</li>
					 
						<li>
                            <?php echo '<a href="../logout.php"><i class="icon-key"></i> Logout </a>';?>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<?php
}
else
{	
header("location:../login.php");
}
?>