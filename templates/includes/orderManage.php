<?php
/***********************************************************
 * File Name	: orderManage.php
 ************************************************************/	
class orderManage
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
	}
	public function getReportTitle($frmdate=null,$todate=null){
		extract($_POST);
		$report_title = '';
		switch($report_type){
			case "superstockist":
					$report_title = 'Super Stockist';
				break;
			case "stockist":
					$report_title = 'Stockist';
				break;
			case "salesperson":	
					$report_title = 'Sales Person';
				break;
			case "shop":	
					$report_title = 'Shops';
				break;
			case "product":	
					$report_title = 'Products';
				break;
		}
		switch($selTest){
				case 1:
					$monday = strtotime("last sunday");
					$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
					 
					$sunday = strtotime(date("d-m-Y",$monday)." +6 days");
					 
					$this_week_sd = date("d-m-Y",$monday);
					$this_week_ed = date("d-m-Y",$sunday);
					$date=" Weekly (". $this_week_sd." TO ".$this_week_ed.") Report";
				break;
				case 2:
					$date=" Current Month (".date('F').") Report";
				break;
				case 3:	
					$date=" Report During ($frmdate To $todate)";
				break;
				case 4:
					$date=" Today's (".date('d-m-Y').") Report";
				break;				
				case 5:
					$date="";
				break;
			}
		$report_title = $report_title.$date;
		return $report_title;
	}
	//title for shop
	public function getReportTitleForShop($frmdate=null,$todate=null){
		extract($_POST);
		$report_title = 'Sale Report For Shop : ';
		$sql="SELECT id , name FROM tbl_shops WHERE id= '$selshop' ";
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$report_title.= $row['name'];
			}
		}else{
			$report_title ='No shop found';
		}
		
		return $report_title;
	}
	
	//new method for sp sales report productwise //ganeshfoods
	public function getAllOrders1() {
		
		extract($_POST);
		$selprod= $_POST['selprod'];
		$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
		$limit = '';
		$order_by = '';
		$search_name = '';
		if($actionType=="excel") {
			$start = 0;
			if($page == 0)
				$start = '';
			else
			{
				$start = ($per_page * ($page)).",";
			}
			$sort = explode(',',$sort_complete);
			$sort_by = '';
			if($sort[0] == 0)
				$sort_by = ' Name '.$sort[1];
			else if($sort[0] == 1)
				$sort_by = ' Total_Sales '.$sort[1];
				
			$order_by = ' ORDER BY '.$sort_by;
			if($per_page != -1)	
				$limit = " LIMIT $start ".$per_page; 
		}
		$condnsearch="";
		$selperiod=$selTest;
		if($selperiod!=0){
			switch($selperiod){
				case 1:
					$condnsearch=" AND yearweek(OA.order_date) = yearweek(curdate())";
					//$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(OA.order_date, '%m')=date_format(now(), '%m')";
					//$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(OA.order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				//	$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(OA.order_date, '%d-%m-%Y') = '".$date."' ";
				//	$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		$where="";
		if($selprod!=''){
			$where= " AND VO.productid = $selprod ";
		}
		if($user_type=='Admin'){
			$external_id = '';
		}
		if($user_type=='Superstockist'){
			$user_typedis="Distributor";
				$getdistrib="SELECT GROUP_CONCAT(id) as ids FROM `tbl_user` where external_id='".$_SESSION[SESSION_PREFIX.'user_id']."' AND user_type ='$user_typedis' group by external_id";
				$resultdistrib = mysqli_query($this->local_connection,$getdistrib);
				//$resultdistrib = mysqli_query($con,$getdistrib);
				$rowdistrib = mysqli_fetch_array($resultdistrib);
				$variable=explode(",", $rowdistrib['ids']);
				$variable1 = "'".implode("','", $variable)."'";
				$tuser=$variable1.",'".$_SESSION[SESSION_PREFIX.'user_id']."'";
				//$user_type="SalesPerson";
				//$sql="SELECT * FROM `tbl_user` where external_id IN(".$tuser.") AND user_type='".$user_type."'";
				$external_id = $tuser;
		}
		if($user_type=='Distributor'){
			$external_id = $_SESSION[SESSION_PREFIX.'user_id'];
		}
		
		if($external_id != '')
		{
			$where = " AND salesman.external_id IN (". $external_id.")";
		}
		 $sql = "SELECT
					salesman.id,	
					salesman.firstname,
					sum(VO.variantunit) as totalunit, 
					sum(VO.variantunit * VO.totalcost) as Total_Sales
				FROM tbl_user salesman 
				LEFT JOIN tbl_order_app OA ON OA.order_by = salesman.id
				LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid
				LEFT JOIN tbl_product TP ON VO.productid = TP.id				
				WHERE salesman.user_type='salesperson'
					$where $condnsearch 
					 group by salesman.id ";	//date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //tbl_order_app
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	//new method for shop sales report  //ganeshfoods
	public function getAllOrders2() {
		extract($_POST);
		$selshop= $_POST['selshop'];
		
		$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
		$luser_id = $_SESSION[SESSION_PREFIX.'user_id'];
		$where="";		
		if($user_type=='Superstockist'){
			$where=" AND OA.superstockistid ='$luser_id' ";
		}
		if($user_type=='Distributor'){
			$where=" AND OA.distributorid='$luser_id' ";
		}
		
		 $sql = "SELECT					
					TP.id as id,	
					TP.productname as productname,
					date_format(OA.order_date, '%M') as month,
					VO.weightquantity,
					VO.unit as unit, 
					VO.variantunit as variantunit, 
					VO.totalcost as unitcost,
					VO.product_varient_id
				FROM tbl_shops AS TS
				LEFT JOIN tbl_order_app OA ON OA.shop_id = TS.id							
				LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid
				LEFT JOIN tbl_product TP ON VO.productid = TP.id 
				WHERE OA.shop_id = $selshop
				$where ORDER BY `TP`.`productname`";	//date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //FROM tbl_product TP 
			
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	
	public function getAllOrders() {
		extract($_POST);	
		$limit = '';
		$order_by = '';
		$search_name = '';
		if($actionType=="excel") {
			$start = 0;
			if($page == 0)
				$start = '';
			else
			{
				$start = ($per_page * ($page)).",";
			}
			$sort = explode(',',$sort_complete);
			$sort_by = '';
			if($sort[0] == 0)
				$sort_by = ' Name '.$sort[1];
			else if($sort[0] == 1)
				$sort_by = ' Total_Sales '.$sort[1];
				
			$order_by = ' ORDER BY '.$sort_by;
			if($per_page != -1)	
				$limit = " LIMIT $start ".$per_page; 
		}
		$condnsearch="";
		$selperiod=$selTest;
		if($selperiod!=0){
			switch($selperiod){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
					$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
					$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($report_type){
			case "superstockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' $search_name
							GROUP BY u.firstname  ".$order_by.$limit;
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name
							GROUP BY u.firstname ".$order_by.$limit;
						break;
						 
						break;
					}
				break;
			case "stockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.distributorid = u.id ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Superstockist":						
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.distributorid = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' AND u.external_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						 
						break;
					}
				break;
			case "salesperson":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id, u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.order_by = u.id ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE u.user_type='SalesPerson' $search_name GROUP BY u.firstname ".$order_by.$limit;	
							
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND (u.external_id IN (  '".$_SESSION[SESSION_PREFIX.'user_id']."') OR u.external_id IN (SELECT id FROM tbl_user where external_id='".$_SESSION[SESSION_PREFIX.'user_id']."') ) 
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_order_app oa 
							LEFT JOIN tbl_user u  ON oa.order_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv, tbl_shops AS ss WHERE ss.id=sv.shop_id AND u.id = sv.salesperson_id AND (ss.stockist_id = ".$_SESSION[SESSION_PREFIX.'user_id'].") $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND (u.external_id IN (".$_SESSION[SESSION_PREFIX.'user_id'].")  OR external_id LIKE ('%,". $_SESSION[SESSION_PREFIX.'user_id']."%'))
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_order_app oa 
							LEFT JOIN tbl_user u  ON oa.order_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name   AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
					}
				break;
			case "shop":	
					if($search != '')
						$search_name = " AND s.name LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":						
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_order_app oa ON shop_id = s.id
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) ".$condnsearch." 
							WHERE s.id!=0 AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_order_app oa ON shop_id = s.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE s.id!=0 AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !=''  $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_order_app oa ON shop_id = s.id  AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE s.id!=0  AND  s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
							
						break;
					}	
				break;
			case "product":	
					if($search != '')
						$search_name = " AND p.productname LIKE '%".$search."%'";
				switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":		
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							$sql="SELECT p.productname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_order_app oa ON oa.catid   = p.catid  ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))  AND vo.productid = p.id 
							WHERE p.id!=0 AND p.productname !=''  $search_name 
							GROUP BY p.productname ".$order_by.$limit;		
						break;
						case "Superstockist":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							$sql="SELECT p.productname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_order_app oa ON oa.catid   = p.catid AND  oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name 
							GROUP BY p.productname ".$order_by.$limit;		
						break;
						case "Distributor":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							$sql="SELECT p.productname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_order_app oa ON oa.catid   = p.catid AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name  
							GROUP BY p.productname ".$order_by.$limit;							
						break;
					}	
				break;
			default:
				break;
		}
		
		
		
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			if($sql2 != ''){
				$result2 = mysqli_query($this->local_connection,$sql2);
				$j = 1;
				$row_count2 = mysqli_num_rows($result2);
				if($row_count2 > 0){
					$name = array_column($records,'Name') ;
					
					while($row2 = mysqli_fetch_assoc($result2))
					{
						if(!in_array($row2['Name'],$name))
						{
							$records2[$j] = $row2;
							$j++;
						}
					}				
				}else{
					$records2 =0;
				}
			}
			if($records2 != 0)
			{
				$records = array_merge($records,$records2);				
			}
			return $records;
		}else{
			return 0;
		}
	}
	public function getReportTitleForSP($selTest=null,$frmdate=null,$todate=null){	
		$report_title = '';		
		switch($selTest){
				case 1:
					$monday = strtotime("last sunday");
					$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
					 
					$sunday = strtotime(date("d-m-Y",$monday)." +6 days");
					 
					$this_week_sd = date("d-m-Y",$monday);
					$this_week_ed = date("d-m-Y",$sunday);
					$date=" Weekly (". $this_week_sd." TO ".$this_week_ed.") Report";
				break;
				case 2:
					$date=" Current Month (".date('F').") Report";
				break;
				case 3:	
					$frmdate = date('d-m-Y',$frmdate);
					$todate = date('d-m-Y',$todate);
					$date=" Report During ($frmdate To $todate)";
				break;
				case 4:
					$date=" Today's (".date('d-m-Y').") Report";
				break;				
				case 5:
					$date="";
				break;
			}
		$report_title = $report_title.$date;
		return $report_title;
	}
	function getSPShopOrdersSummary($sp_id, $filter_date,$date1=null,$date2=null){
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.variantunit * vo.totalcost) as Total_Sales
				FROM tbl_user u 				
				LEFT JOIN tbl_order_app oa ON oa.order_by = u.id ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
			case "Superstockist":	
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.variantunit * vo.totalcost) as Total_Sales
				FROM tbl_user u 
				LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
			case "Distributor":		
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.variantunit * vo.totalcost) as Total_Sales
				FROM tbl_user u  
				LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
		}
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_array($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	function getSPShopOrders($sp_id, $s_id, $filter_date,$date1=null,$date2=null){
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";					
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";					
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";					
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
				$sql="SELECT oa.brandnm, oa.categorynm, vo.product_varient_id, vo.orderappid, vo.variantunit, vo.totalcost, vo.id as variant_oder_id, oa.order_date, vo.orderid, vo.productid, vo.campaign_sale_type
				FROM tbl_order_app oa 				
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid)
				WHERE oa.order_by = $sp_id AND oa.shop_id = $s_id ".$condnsearch." ORDER BY vo.id DESC";
			break;
			case "Superstockist":
				$sql="SELECT oa.brandnm, oa.categorynm, vo.product_varient_id, vo.orderappid, vo.variantunit, vo.totalcost, vo.id as variant_oder_id, oa.order_date, vo.orderid, vo.productid, vo.campaign_sale_type
				FROM tbl_order_app oa 		
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid )
				WHERE oa.order_by = $sp_id AND vo.status != 1 AND oa.shop_id = $s_id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." ORDER BY vo.id DESC";
			break;
			case "Distributor":	
				$sql="SELECT oa.brandnm, oa.categorynm, vo.product_varient_id, vo.orderappid, vo.variantunit, vo.totalcost, vo.id as variant_oder_id, oa.order_date, vo.orderid, vo.productid, vo.campaign_sale_type
				FROM tbl_order_app oa 		
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid)
				WHERE oa.order_by = $sp_id AND oa.shop_id = $s_id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." ORDER BY vo.id DESC";
			break;
		}
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_array($result1))
			{
				$sql_product = "SELECT  productname FROM `tbl_product` WHERE id = '".$row['productid'] ."'";										
				$product_result = mysqli_query($this->local_connection,$sql_product);
				$obj_product = mysqli_fetch_object($product_result);
				$product_varient_id = $row['product_varient_id'];
				$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
				$resultprd = mysqli_query($this->local_connection,$sqlprd);
				$rowprd = mysqli_fetch_array($resultprd);
				$exp_variant1 = $rowprd['variant_1'];
				$imp_variant1= split(',',$exp_variant1);
				$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
				$resultunit = mysqli_query($this->local_connection,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit1 = $rowunit['unitname'];
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
				$resultunit = mysqli_query($this->local_connection,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit2 = $rowunit['unitname'];
				
				//dynamic variant name also
				$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
					left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant1[1]'";
				$resultvariant = mysqli_query($this->local_connection,$sql);
				$rowvariant = mysqli_fetch_array($resultvariant);
				$variant_variant1 = $rowvariant['name'];
				
				$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
					left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant2[1]'";
				$resultvariant = mysqli_query($this->local_connection,$sql);
				$rowvariant = mysqli_fetch_array($resultvariant);
				$variant_variant2 = $rowvariant['name']; 
				
				$commonObjctype=0;
				$sqlforclient = "select client_type from  tbl_clients_maintenance where id=".COMPID." limit 1 ";
				$resultforclient= mysqli_query($this->common_connection,$sqlforclient);	
				$resultforclient1 = mysqli_fetch_array($resultforclient);
				$commonObjctype= $resultforclient1['client_type'];
				
				$dimentionDetails = "";
				if($commonObjctype!='1'){
					if($imp_variant1[0]!="") {
					$dimentionDetails = " - ".$variant_variant1.": " .  $imp_variant1[0] . " " . $variant_unit1;
					}
					if($imp_variant2[0]!="") {
						$dimentionDetails .= " - ".$variant_variant2." : " .  $imp_variant2[0] . " " . $variant_unit2;
					}
				}else{
					if($variant_unit2!="") {
						$dimentionDetails = " - ".$variant_variant2." : " . $variant_unit2;
					}
				}
				
				/* 
				$dimentionDetails = "";
				if($variant_unit1!="") {
					$dimentionDetails = " - " . $variant_unit1;//" .  $imp_variant1[0] . " 
				} */
				//$dimentionDetails .= " - Weight: " .  $imp_variant2[0] . " " . $variant_unit2;
				$row['prodname_variant'] = $obj_product->productname . $dimentionDetails;
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	public function no_order_history_details($sp_id=null,$filter_date=null,$date1=null,$date2=null) {
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');					
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;				
				default:
					$condnsearch_sp="";
				break;

			}
		}
		if($sp_id!=''){
			$where = " AND salesperson_id = $sp_id";
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
					$sql1="SELECT `id`, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit 
					WHERE shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
			case "Superstockist":
					$sql1="SELECT tbl_shop_visit.id, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit INNER JOIN tbl_shops as s ON s.id = shop_id 
					WHERE s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."'
					AND shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
			case "Distributor":	
					$sql1="SELECT tbl_shop_visit.id, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit INNER JOIN tbl_shops as s ON s.id = shop_id 
					WHERE s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."'
					AND shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
		}
		//echo $sql1; exit;
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	function getSalesPOrders_old($date,$salespid){
		echo $sql = "SELECT	DISTINCT(VO.orderid), OA.shop_id, shops.name as shopname, shops.address, shops.mobile,
				OA.order_date , OA.categorynm, oplacelat, oplacelon, VO.orderappid,
				(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
				(SELECT name FROM tbl_state WHERE id = shops.state) AS statename
				FROM tbl_order_app OA 
				LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
				LEFT JOIN tbl_variant_order VO ON VO.orderappid= OA.id				
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.order_by = " . $salespid."
				GROUP BY VO.orderid,
				OA.order_date
				ORDER BY OA.order_date ASC";
//GROUP BY OA.shop_id
				$result = mysqli_query($this->local_connection,$sql); 				
				return $result;		
	}
	function getSalesPOrders($date,$salespid){
			$sql = "(
						SELECT  DISTINCT(VO.orderid), OA.shop_id, shops.name as shopname, 
						shops.address, shops.mobile, OA.order_date , OA.categorynm, 
						oplacelat, oplacelon, VO.orderappid,
						(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
						(SELECT name FROM tbl_state WHERE id = shops.state) AS statename, 
						OA.order_date AS date,
						SV.shop_visit_reason, SV.shop_close_reason_type, SV.shop_close_reason_details,SV.flag,SV.comments
						FROM tbl_order_app OA 
						LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
						LEFT JOIN tbl_variant_order VO ON VO.orderappid= OA.id		
						LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id AND OA.order_date =SV.shop_visit_date_time
						WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.order_by = $salespid
						GROUP BY VO.orderid,
						OA.order_date
						ORDER BY OA.order_date ASC
					)
					UNION  ALL
					(
						SELECT DISTINCT(VO.orderid), OA.shop_id, 
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname,
						shops.address, shops.mobile,
						OA.order_date , OA.categorynm, oplacelat, oplacelon, VO.orderappid,
						(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
						(SELECT name FROM tbl_state WHERE id = shops.state) AS statename, 
						SV.shop_visit_date_time AS date,						
						SV.shop_visit_reason, SV.shop_close_reason_type, SV.shop_close_reason_details,SV.flag,SV.comments
						FROM tbl_shop_visit AS SV
						LEFT OUTER JOIN  tbl_order_app OA  ON OA.shop_id = SV.shop_id AND OA.order_date =SV.shop_visit_date_time					
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
						LEFT JOIN tbl_variant_order VO ON VO.orderappid= OA.id	
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = $salespid
						ORDER BY SV.shop_visit_date_time ASC
					)
					ORDER BY date ASC";
//GROUP BY OA.shop_id
				$result = mysqli_query($this->local_connection,$sql); 				
				return $result;		
	}
	function getSalesPOrdersProducts($date,$salespid)
	{
		$sql = "SELECT DISTINCT(OV.product_varient_id), OV.product_varient_id, OA.brandnm, OA.categorynm,
		(SELECT  productname FROM `tbl_product` WHERE id = OV.productid) AS productname
		FROM `tbl_order_app` AS OA 
		LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id 
		WHERE
		date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.order_by = " . $salespid;
									
		$result = mysqli_query($this->local_connection,$sql);
		return $result;		
	}
	function getSalesPOrdersPQuantity($shop_id,$product_varient_id,$order_date)
	{
		$sql = "SELECT OA.quantity, OV.totalcost, OV.variantunit
		FROM `tbl_order_app` AS OA 
		LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id 
		WHERE
		OV.product_varient_id = ".$product_varient_id." AND OA.shop_id = " . $shop_id." AND OA.order_date = '".$order_date."'";
									
		$result = mysqli_query($this->local_connection,$sql);
		$row = mysqli_fetch_assoc($result);
		return $row;		
	}
	function getSalesPOrdersDetails($orderRecId){
		$sql = "SELECT	shops.name as shopname, shops.address, shops.mobile,
				OA.order_date , OA.categorynm,
				(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
				(SELECT name FROM tbl_state WHERE id = shops.state) AS statename
				FROM tbl_order_app OA 
				LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id			
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.order_by = " . $salespid."
				ORDER BY OA.order_date ASC";

				$result = mysqli_query($this->local_connection,$sql); 				
				return $result;		
	}
	function getSalesPOrdersStart_old($date,$salespid){
		$sql = "SELECT	OA.order_date , VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon
				FROM tbl_order_app OA 
				LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid 
				LEFT JOIN tbl_shops shops ON shops.id= VO.shopid			
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.order_by = " . $salespid."
				ORDER BY OA.order_date ASC LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 	
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPOrdersEnd_old($date,$salespid){
		$sql = "SELECT	OA.order_date, VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon
				FROM tbl_order_app OA 
				LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid 
				LEFT JOIN tbl_shops shops ON shops.id= VO.shopid			
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.order_by = " . $salespid."
				ORDER BY OA.order_date DESC LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 				
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPOrdersStart($date,$salespid){				
					$sql = "(
						SELECT  VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon,
						OA.order_date AS date
						FROM tbl_order_app OA 
						LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
						LEFT JOIN tbl_variant_order VO ON VO.orderappid= OA.id		
						LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id  AND OA.order_date =SV.shop_visit_date_time
						WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.order_by = " . $salespid."
						GROUP BY VO.orderid,
						OA.order_date
						ORDER BY OA.order_date ASC
					)
					UNION  ALL
					(
						SELECT VO.id,
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname, 
						shops.address, OA.oplacelat, OA.oplacelon,
						SV.shop_visit_date_time AS date		
						FROM tbl_shop_visit AS SV
						LEFT OUTER JOIN  tbl_order_app OA  ON OA.shop_id = SV.shop_id  AND OA.order_date =SV.shop_visit_date_time						
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
						LEFT JOIN tbl_variant_order VO ON VO.orderappid= OA.id	
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = " . $salespid."
						ORDER BY SV.shop_visit_date_time ASC
					)
					ORDER BY date ASC LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 	
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPOrdersEnd($date,$salespid){
					$sql = "(
						SELECT  OA.id AS oid, VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon,
						OA.order_date AS date
						FROM tbl_order_app OA 
						LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
						LEFT JOIN tbl_variant_order VO ON VO.orderappid= OA.id		
						LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id  AND OA.order_date =SV.shop_visit_date_time
						WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.order_by = $salespid
						GROUP BY VO.orderid,
						OA.order_date
						ORDER BY OA.id DESC, OA.order_date DESC
					)
					UNION  ALL
					(
						SELECT OA.id AS oid, VO.id,
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname, 
						shops.address, OA.oplacelat, OA.oplacelon,
						SV.shop_visit_date_time AS date		
						FROM tbl_shop_visit AS SV
						LEFT OUTER JOIN  tbl_order_app OA  ON OA.shop_id = SV.shop_id  AND OA.order_date =SV.shop_visit_date_time						
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
						LEFT JOIN tbl_variant_order VO ON VO.orderappid= OA.id	
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = $salespid
						ORDER BY SV.id DESC, SV.shop_visit_date_time DESC
					)
					ORDER BY date DESC, oid DESC  LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 				
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPStartEndDay($date,$salespid){
		$sql_day_time = "SELECT	`sp_id`, `tdate`, `presenty`, `dayendtime`
				FROM tbl_sp_attendance
				WHERE
				date_format(tdate, '%d-%m-%Y') = '".$date."' AND sp_id = " . $salespid;

		$result = mysqli_query($this->local_connection,$sql_day_time); 
		$totalRecords=mysqli_num_rows($result);
		if($totalRecords > 0)
					return mysqli_fetch_array($result);
				else
					return $totalRecords;	
	}
	function getSProductVariant($product_varient_id){
		$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
		$resultprd = mysqli_query($this->local_connection,$sqlprd);
		$rowprd = mysqli_fetch_array($resultprd);
		$exp_variant1 = $rowprd['variant_1'];
		$imp_variant1= split(',',$exp_variant1);
		$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);
		$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
		$resultunit = mysqli_query($this->local_connection,$sql);
		$rowunit = mysqli_fetch_array($resultunit);
		$variant_unit1 = $rowunit['unitname'];
		$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
		$resultunit = mysqli_query($this->local_connection,$sql);
		$rowunit = mysqli_fetch_array($resultunit);
		$variant_unit2 = $rowunit['unitname'];
		
		//dynamic variant name also
		$sql = "select client_type from  tbl_clients_maintenance where id=".COMPID." limit 1 ";
		$result= mysqli_query($this->common_connection,$sql);	
		$result1 = mysqli_fetch_array($result);
		$commonObjctype = $result1['client_type'];
		
		$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
			left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant1[1]'";
		$resultvariant = mysqli_query($this->local_connection,$sql);
		$rowvariant = mysqli_fetch_array($resultvariant);
		$variant_variant1 = $rowvariant['name'];
		
		$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
			left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant2[1]'";
		$resultvariant = mysqli_query($this->local_connection,$sql);
		$rowvariant = mysqli_fetch_array($resultvariant);
		$variant_variant2 = $rowvariant['name']; 
		$dimentionDetails = "";
		if($commonObjctype!='1'){
			if($imp_variant1[0]!="") {
			$dimentionDetails = " - ".$variant_variant1.": " .  $imp_variant1[0] . " " . $variant_unit1;
			}
			if($imp_variant2[0]!="") {
				$dimentionDetails .= " - ".$variant_variant2." : " .  $imp_variant2[0] . " " . $variant_unit2;
			}
		}else{
			if($variant_unit2!="") {
				$dimentionDetails = " - ".$variant_variant2." : " . $variant_unit2;
			}
		}
		return $dimentionDetails;
	}
	public function getLocation($latitude, $longitude){
		
		$geolocation = $latitude.",".$longitude;//"18.4773911,73.9025829";//
		$request = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$geolocation&sensor=false";
		
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $request); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch);   

		//$output = file_get_contents($request);
		$json_decode = json_decode($output);
		//print"<pre>";
		//print_R($json_decode);
		$address = "";
		if(isset($json_decode->results[0])) {
			$response = array();
			$address = $json_decode->results[0]->formatted_address;
		}
		return $address;
			/*foreach($json_decode->results[0]->address_components as $addressComponet) {
				if(in_array('political', $addressComponet->types)) {
						$response[] = $addressComponet->long_name; 
				}
			}

			if(isset($response[0])){ $first  =  $response[0];  } else { $first  = 'null'; }
			if(isset($response[1])){ $second =  $response[1];  } else { $second = 'null'; } 
			if(isset($response[2])){ $third  =  $response[2];  } else { $third  = 'null'; }
			if(isset($response[3])){ $fourth =  $response[3];  } else { $fourth = 'null'; }
			if(isset($response[4])){ $fifth  =  $response[4];  } else { $fifth  = 'null'; }

			if( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth != 'null' ) {
				echo "<br/>Address:: ".$first;
				echo "<br/>City:: ".$second;
				echo "<br/>State:: ".$fourth;
				echo "<br/>Country:: ".$fifth;
			}
			else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth == 'null'  ) {
				echo "<br/>Address:: ".$first;
				echo "<br/>City:: ".$second;
				echo "<br/>State:: ".$third;
				echo "<br/>Country:: ".$fourth;
			}
			else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth == 'null' && $fifth == 'null' ) {
				echo "<br/>City:: ".$first;
				echo "<br/>State:: ".$second;
				echo "<br/>Country:: ".$third;
			}
			else if ( $first != 'null' && $second != 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
				echo "<br/>State:: ".$first;
				echo "<br/>Country:: ".$second;
			}
			else if ( $first != 'null' && $second == 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
				echo "<br/>Country:: ".$first;
			}
		  }*/
	}
}
?>