<?php
include ("../../includes/config.php");
include "../includes/commonManage.php";	
$commonObj 	= 	new commonManage($con,$conmain);
$commonObjctype 	= 	$commonObj->log_get_commonclienttype($con,$conmain);

$productid=$_GET["cat_id"];
$select_name_id = "dropdownProductVariant";
if(isset($_GET['select_name_id'])){
	$select_name = $_GET['select_name_id'];
	$select_id = $_GET['select_name_id'];
}

$selectedval = "";
if(isset($_GET['selectedval']))
	$selectedval = $_GET['selectedval'];

$multiple = "";
if(isset($_GET['multiple']))
	$multiple = 'multiple';

if($multiple != '')
{
	$select_name = $_GET['select_name_id']."[]";
	$select_id = $_GET['select_name_id'];
}

$function_name = "";
if(isset($_GET['function_name'])){
	$function_name = 'onchange="'.$_GET['function_name'].'(this)";';
}
?>
<select name="<?php echo $select_name; ?>" id="<?php echo $select_id; ?>" class="form-control" <?=$multiple;?> <?=$function_name;?>>
<?php if(isset($_GET['multiple'])){ ?>
	<option value="select_all">-Select All-</option>
<?php }else{ ?>
	<option value="">-Select-</option>
	<?php	}										
	if($productid!="") {
		$sql="SELECT * from `tbl_product_variant` where productid = $productid";
		$result1 = mysqli_query($con,$sql);
		$i = 1;
		while($row = mysqli_fetch_array($result1))
		{
			$exp_variant1 = $row['variant_1'];
			$imp_variant1= split(',',$exp_variant1);
			
			if($exp_variant1 != '')
			{
				$sql1="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
				$result2 = mysqli_query($con,$sql1);
				$row_prd_variant1 = mysqli_fetch_array($result2);
				$combine1 = $row['id']." ".$imp_variant1[0]." ".$row_prd_variant1['id'];
				$combine1_display = "";
				if($imp_variant1[0]!="")
					$combine1_display = " (" . $imp_variant1[0]." ".$row_prd_variant1['unitname'] . ")";				
			}
			$exp_variant2 = $row['variant_2'];
			$imp_variant2= split(',',$exp_variant2);
			if($exp_variant2 != '')
			{
				$sql2="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant2[1];
				$result3 = mysqli_query($con,$sql2);
				$row_prd_variant2 = mysqli_fetch_array($result3);
				
				$combine2 = $row['id'];	
				$combine2_display = $imp_variant2[0]." ".$row_prd_variant2['unitname'];						
				$selected = "";
				if($combine2==$selectedval)
					$selected = "selected";
				echo "<option value='".$combine2."' $selected>" .$combine2_display.$combine1_display ."</option>";
			}
		}
	} ?>
</select>
<?  mysqli_close($con); ?>