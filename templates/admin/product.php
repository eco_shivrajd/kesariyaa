<?php include "../includes/grid_header.php";
include "../includes/commonManage.php";	
$commonObj 	= 	new commonManage($con,$conmain);
$commonObjctype 	= 	$commonObj->log_get_commonclienttype($con,$conmain);
//echo "sdfsd";die(); 
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Product";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Product
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Product</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Product Listing</div>				
							<!--<a href="product_variant.php" class="btn btn-sm btn-default pull-right mt5 ml10">Add Product dimensions</a>-->
							<a href="product-add.php" class="btn btn-sm btn-default pull-right mt5 ">Add Product</a>				  
							<div class="clearfix"></div>
							
						</div>
						
						<div class="portlet-body">				
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>					
										<th>
											 Category
										</th>
										<th>
											 Product Name
										</th>
										<th>
											 Product Price
										</th>
										<th>
											 Product Variant
										</th>
										<th>
											 Product Image
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
									 $sql="SELECT a.categorynm,b.productname,b.id  FROM tbl_category a,tbl_product b where a.id=b.catid";
									$result = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result))
									{?>
										<tr class="odd gradeX">
										<td>
										<?php echo fnStringToHTML($row['categorynm']);?>
										</td>
										<td>
										<a href="product1.php?id=<?php echo $row['id'];?>"><?php echo fnStringToHTML($row['productname']);?></a>
										</td>
										<td>
										<?php $prodid=$row['id'];
										$sqlvarimg="SELECT price FROM tbl_product_variant where productid='$prodid'";
									$resultvarimg = mysqli_query($con,$sqlvarimg);
									while($rowvarimg = mysqli_fetch_array($resultvarimg)){
									echo fnStringToHTML($rowvarimg['price'])."<br>"; } ?>
										</td>
										<td>
										<?php $sqlvarimg="SELECT variant_1,variant_2 FROM tbl_product_variant a where productid='$prodid'";
									$resultvarimg = mysqli_query($con,$sqlvarimg);
									while($rowvarimg = mysqli_fetch_array($resultvarimg)){
										//echo $rowvarimg['variant_2'];
										$imp_variant1= explode(',',$rowvarimg['variant_1']);
										//echo "123  <pre>";print_r($imp_variant1);
										$var_unit_id=trim($rowvarimg['variant_1'],",");
										$sqlvarunit="SELECT unitname FROM tbl_units  where id='".$imp_variant1[1]."'";
										$resultvarunit = mysqli_query($con,$sqlvarunit);
										while($rowvarunit= mysqli_fetch_array($resultvarunit)){
											if($imp_variant1[0]!='' && $commonObjctype!='1'){
												echo $imp_variant1[0]."-".$rowvarunit['unitname']." ";
											}
										}
										
										$imp_variant2= explode(',',$rowvarimg['variant_2']);
										//echo "123  <pre>";print_r($imp_variant1);
										$var_unit_id=trim($rowvarimg['variant_2'],",");
										$sqlvarunit="SELECT unitname FROM tbl_units  where id='".$imp_variant2[1]."'";
										$resultvarunit = mysqli_query($con,$sqlvarunit);
										while($rowvarunit= mysqli_fetch_array($resultvarunit)){
											if($imp_variant2[0]!='' && $commonObjctype!='1'){
												echo $imp_variant2[0]."-".$rowvarunit['unitname']."  ";
											}
										}
										echo "<br>";
									}
										?>
										</td>
									<td>
										<?php 
										$sqlvarimg="SELECT productimage,price,variant_1 FROM tbl_product_variant where productid='$prodid'";
									$resultvarimg = mysqli_query($con,$sqlvarimg);
									while($rowvarimg = mysqli_fetch_array($resultvarimg)){
											if(!empty($rowvarimg['productimage'])){  ?>
										<img src="upload/<?php echo $rowvarimg['productimage'];?>" alt=<?php echo $rowvarimg['productimage'];?> width="100px" />
									<?php } }
										
										 ?>										 
										</td>
										
									<?php } ?>							
								</tbody>
							</table>
						</div>
					</div> 
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>