<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/userManage.php";
include "../includes/shopManage.php";
$userObj 	= 	new userManager($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
$unassignedshop = $shopObj->getShoplistToShopkeeper();
//$userObj->migration_sp_sstockist(); exit;
	
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Shopkeeper";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Shopkeeper
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Shopkeeper</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Shopkeepers Listing
							</div>
							
                            <a  <?php if($unassignedshop!=0){ ?> href="Shopkeeper-add.php"  <?php }else{ ?> disabled <?php } ?> class="btn btn-sm btn-default pull-right mt5">
                                Add Shopkeeper
                              </a>
							
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th width="15%">
									 Name
								</th>	
								<th width="25%">
									Assigned Shop
								</th>					
                                <th width="20%">
									 Email
								</th>
                                <th width="10%">
									 Mobile Number
								</th>
								<th width="10%">
                                	City
                                </th> 
								<th width="10%">
                                	Area
                                </th>    
								<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ ?>
								<th width="10%">
                                	Action
                                </th>
								<?php } ?>
							</tr>
							</thead>
							<tbody>
							<?php
							switch($_SESSION[SESSION_PREFIX.'user_type']){
								case "Admin":									
									$user_type="SalesPerson";									
									$external_id = "";
									break;
								case "Superstockist":
									$user_type="SalesPerson";		
									$external_id = $_SESSION[SESSION_PREFIX.'user_id'];									
								break;
								case "Distributor":								
									$user_type="SalesPerson";									
									$external_id = $_SESSION[SESSION_PREFIX.'user_id'];
								break;
							}
							$user_type='Shopkeeper';
							$result1 = $userObj->getAllLocalUserDetails($user_type,$external_id);
							//echo "<pre>";print_r($result1);die();
						
							while($row = mysqli_fetch_array($result1))
							{ $user_id=$row['id']; ?>
							<tr class="odd gradeX">
								<td>
									 <a href="Shopkeeper-edit.php?id=<?php echo $user_id;?>"><?php echo fnStringToHTML($row['firstname']);?></a>
								</td>
								<?php 	
								$resultshop = $shopObj->getShopAssignedToShopkeeper($user_id);
								?>
								<td><?php echo $resultshop['name'];?></td>
								
								<td><?php echo $row['email']?></td>
								
                                <td><?php echo $row['mobile']?></td>
								<td><?php 
								$city_id=$resultshop['city'];						
								 $sql="SELECT * FROM tbl_city where id = $city_id";
								$result = mysqli_query($con,$sql);
								while($num = mysqli_fetch_array($result))
								{ 
									echo  $num['name'];
								}									
								?>
								</td>
								<td><?php 
								$suburbid=$resultshop['suburbid'];						
								 $sql="SELECT * FROM tbl_surb where id = $suburbid";
								$result = mysqli_query($con,$sql);
								while($num = mysqli_fetch_array($result))
								{ 
									echo  $num['suburbnm'];
								}									
								?>
								</td>
								<td><a href="manageuser.php?utype=Shopkeeper&id=<?php echo $row['id']?>">Delete</a></td>
							</tr>
							<?php } ?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->

</body>
<!-- END BODY -->
</html>