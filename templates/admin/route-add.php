<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageDeliveries"; $activeMenu = "Routes";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Route
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="routes.php">Route</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Route</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Route
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						  
						
					<form id="addrouteform" class="form-horizontal" data-parsley-validate="" role="form" method="post">
						
						<div class="form-group">
							<label class="col-md-3">State:<span class="mandatory">*</span></label>
							<div class="col-md-4">
							<select name="state" id="state" 
							data-parsley-trigger="change" 
							data-parsley-required="#true" 
							data-parsley-required-message="Please select state" 
							class="form-control" onchange="fnShowCity(this.value)">
							<option value="">-select-</option>
							<option value="1">Andaman and Nicobar Islands</option>
							<option value="2">Andhra Pradesh</option><option value="3">Arunachal Pradesh</option><option value="4">Assam</option><option value="5">Bihar</option><option value="6">Chandigarh</option><option value="7">Chhattisgarh</option><option value="8">Dadra and Nagar Haveli</option><option value="9">Daman and Diu</option><option value="10">Delhi</option><option value="11">Goa</option><option value="12">Gujarat</option><option value="13">Haryana</option><option value="14">Himachal Pradesh</option><option value="15">Jammu and Kashmir</option><option value="16">Jharkhand</option><option value="17">Karnataka</option><option value="18">Kenmore</option><option value="19">Kerala</option><option value="20">Lakshadweep</option><option value="21">Madhya Pradesh</option><option value="22">Maharashtra</option><option value="23">Manipur</option><option value="24">Meghalaya</option><option value="25">Mizoram</option><option value="26">Nagaland</option><option value="27">Narora</option><option value="28">Natwar</option><option value="29">Odisha</option><option value="30">Paschim Medinipur</option><option value="31">Pondicherry</option><option value="32">Punjab</option><option value="33">Rajasthan</option><option value="34">Sikkim</option><option value="35">Tamil Nadu</option><option value="36">Telangana</option><option value="37">Tripura</option><option value="38">Uttar Pradesh</option><option value="39">Uttarakhand</option><option value="41">West Bengal</option>							</select>
							</div>
							</div>			
							<div class="form-group" id="city_div" style="display:none;">
							  <label class="col-md-3">City:<span class="mandatory">*</span></label>
							  <div class="col-md-4" id="div_select_city">
							  <select name="city" id="city" data-parsley-trigger="change" class="form-control">
								<option selected="" value="">-Select-</option>										
								</select>
							  </div>
							</div><!-- /.form-group -->
							<div class="form-group" id="area_div" style="display:none;">
							  <label class="col-md-3">Area:<span class="mandatory">*</span></label>
							  <div class="col-md-4" id="div_select_area">
							  <select name="area" id="area"
							  data-parsley-trigger="change"
							  class="form-control">
								<option selected="" value="">-Select-</option>									
								</select>
							  </div>
							</div><!-- /.form-group --> 						
							<div class="form-group" id="subarea_div" style="display:none;">
							  <label class="col-md-3">Subarea:</label>
							  <div class="col-md-4" id="div_select_subarea">
							  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
								<option selected="" value="">-Select-</option>									
								</select>
							  </div>
							</div><!-- /.form-group --> 
						<div class="form-group">
						  <label class="col-md-3">Route Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text" 
							placeholder="Enter Route Name" 
							data-parsley-trigger="change" 
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Route name" 
							data-parsley-maxlength="50" 
							data-parsley-maxlength-message="Only 50 characters are allowed" 
							name="routename" id="routename" class="form-control">
						  </div>
						  <input type="hidden" id="shopids"
							name="shopids" value="">
						</div><!-- /.form-group -->
						
						
					
						<div class="form-group">
							<label class="col-md-3">Assign Shops:</label>
							<div class="col-md-9">
								<div class="row">
									<div class="col-sm-12">
										<p> Select shop record to drag and drop to assigned list</p>
									</div>
									<div class="col-sm-4">
										<p><b>Shop(s)</b></p>
										<ul id="shopslist" class="list-group list-group-sortable-connected maxhts">
										</ul>
									</div>
									
									<div class="col-sm-4">
										<p><b>Assigned Shop</b></p>
										<ul id="shopsassign" class="list-group list-group-sortable-connected maxhts">
											
										</ul>
									</div>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="button" name="submit" id="submit" class="btn btn-primary" onclick="javascript: return addroute()">Submit</button>
							<a href="routes.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
						
					  </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<script>
jQuery(document).ready(function() {
    $('.list-group-sortable-connected').sortable({
        placeholderClass: 'list-group-item',
        connectWith: '.connected'
    });
    addElements();
});

function addroute() {
	var $form = $('#addrouteform');
	$form.parsley().validate();
    var ids = "";
    $('#shopsassign li').each(function(i) {
        ids += $(this).attr('id').toString() + ",";
    });
    if ($('#shopsassign li').length < 1) {
        //$('#shopsassign').sortable('cancel');
        alert("Atleast one Route required to assign!Route not added.");
    } else {
        var routename = $('#routename').val();
        //
        if (routename != '' && ids != '') {
            var url = 'route-save.php?shopids=' + ids + '&routename=' + routename;
            $.ajax({
                type: 'post',
                url: url,
                error: function(data) {
                    console.log(data);
                    alert("Route not added." + data);
                   location.reload();
                },
                success: function(data) {
                    console.log(data);
                    if (data == 'duplicate') {
                        alert("Duplicate route name not allowed.");
                       location.reload();
                    } else {
                        alert("Route added successfully.");
                        window.location = 'routes.php';
                    }

                }
            });
        }
    }
}

function addElements() {
    //$("#shopsassign").empty();
    $("#shopslist").empty();
    $("#shopsassign").empty();
	 var state = $('#state').val();
    var city = $('#city').val();
    var area = $('#area').val();
    var subarea = $('#subarea').val();
   
    var url = 'fetch_shops1.php?state=' + state + '&city=' + city + '&area=' + area + '&subarea=' + subarea;
	//alert(url);
    $.ajax({
        url: url,
        datatype: "JSON",
        contentType: "application/json",
        data: JSON.stringify({
            id: "value",
            name: "value"
        }),
        error: function(data) {
            console.log("error:" + data)
        },
        success: function(data) {
            console.log(data);
            var JSONObject = jQuery.parseJSON(data);

            for (var i = 0; i < JSONObject.length; i++) {

                $("#shopslist").append("<li id=" + JSONObject[i].id + " class='list-group-item'  draggable='true' style='display: block;'>" + JSONObject[i].name + "</li>");

                DragAndDrop();
            }
        }

    });
}

function DragAndDrop() {

    // there's the gallery and the trash
    var $gallery = $("#shopslist");
    var $trash = $("#shopsassign");

    // let the gallery items be draggable
    $("li", $gallery).draggable({
        cancel: "button", // these elements won't initiate dragging
        revert: "invalid", // when not dropped, the item will revert back to its initial position
        containment: "document",
        helper: "clone",
        cursor: "move"
    });
    $("li", $trash).draggable({
        cancel: "button", // these elements won't initiate dragging
        revert: "invalid", // when not dropped, the item will revert back to its initial position
        containment: "document",
        helper: "clone",
        cursor: "move"
    });
    //debugger;
    // let the trash be droppable, accepting the gallery items
    $trash.droppable({
        accept: "#shopslist > li",
        drop: function(event, ui) {
            $("#shopsassign").append(ui.draggable);
            $("li", $trash).draggable({
                cancel: "button", // these elements won't initiate dragging
                revert: "invalid", // when not dropped, the item will revert back to its initial position
                containment: "document",
                helper: "clone",
                cursor: "move"
            })
        }

    });
    $gallery.droppable({
        accept: "#shopsassign > li",
        drop: function(event, ui) {
            $("#shopslist").append(ui.draggable);

            $("li", $gallery).draggable({
                cancel: "button", // these elements won't initiate dragging
                revert: "invalid", // when not dropped, the item will revert back to its initial position
                containment: "document",
                helper: "clone",
                cursor: "move"
            })
        }

    });
}


/* To change city on change of state*/
function calculate_data_count(element_value) {
    element_value = element_value.toString();
    var element_arr = element_value.split(',');
    return element_arr.length;
}

function setSelectNoValue(div, select_element) {
    var select_selement_section = '<select name="' + select_element + '" id="' + select_element + '" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
    document.getElementById(div).innerHTML = select_selement_section;
}

function fnShowCity(id_value) {
    addElements();
    $("#city_div").show();
    $("#area").html('<option value="">-Select-</option>');
    $("#subarea").html('<option value="">-Select-</option>');
    var url = "getCityDropDown.php?cat_id=" + id_value + "&select_name_id=city&mandatory=mandatory";
    CallAJAX(url, "div_select_city");
}

function FnGetSuburbDropDown(id) {
    addElements();
    $("#area_div").show();
    $("#subarea").html('<option value="">-Select-</option>');
    var url = "getSuburDropdown.php?cityId=" + id.value + "&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
    CallAJAX(url, "div_select_area");
}

function FnGetSubareaDropDown(id) {
    addElements();
    var suburb_str = $("#area").val();
    $("#subarea_div").show();
    var url = "getSubareaDropdown.php?area_id=" + id.value + "&select_name_id=subarea&function_name=addElements"; //&function_name=addElements
    CallAJAX(url, "div_select_subarea");
}
</script>
<script src="../../assets/admin/pages/scripts/jquery.sortable.js"></script>
</body>
<!-- END BODY -->
</html>