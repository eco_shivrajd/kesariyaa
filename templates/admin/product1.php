<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";	
?>
<!-- END HEADER -->
<?php
$sqlvariant1="SELECT name FROM tbl_variant WHERE id='40'";
$resultprd1 = mysqli_query($con,$sqlvariant1);
$rowvariant1 = mysqli_fetch_array($resultprd1);
//////////////////////////////////////////////////////////////////////
$sqlvariant2	= "SELECT name FROM tbl_variant WHERE id='41'";
$resultprd2 	= mysqli_query($con,$sqlvariant2);
$rowvariant2 	= mysqli_fetch_array($resultprd2);
/////////////////////////////////////////////////////////////////////
$commonObj 	= 	new commonManage($con,$conmain);
$commonObjctype 	= 	$commonObj->log_get_commonclienttype($con,$conmain);
if(isset($_POST['submit']))
{
	//echo"";var_dump($_POST);
	$id= $_GET['id'];	
	$catid=$_POST['catid'];
	$productname=fnEncodeString($_POST['productname']);
	$sqlprvariant="SELECT variant_cnt FROM `tbl_product_variant` WHERE `productid`='$id' ORDER BY `id` DESC LIMIT 1";
	$result_prvariant = mysqli_query($con,$sqlprvariant);
	$row_prvariant = mysqli_fetch_array($result_prvariant);
	//echo "<pre>";print_r($row_prvariant);
	for($i=1;$i<=$row_prvariant['variant_cnt'];$i++){
		$size	=fnEncodeString($_POST['size_'.$i]).",".fnEncodeString($_POST['cmbUnit_'.$i]);
		$weight=fnEncodeString($_POST['weight_'.$i]).",".fnEncodeString($_POST['cmbweight_'.$i]);
		
		$file=$_FILES['productimage']['name'][$i-1];
		$folder = "upload/";
		$upload_image = $folder . basename($_FILES["productimage"]["name"][$i-1]);
		$str=move_uploaded_file($_FILES["productimage"]["tmp_name"][$i-1], $upload_image);
		
		$barcode_file=$_FILES['productbarcodeimage_'.$i]['name'];
		$upload_barcode_image = $folder . basename($_FILES['productbarcodeimage_'.$i]["name"]);
		$str_barcode=move_uploaded_file($_FILES["productbarcodeimage_".$i]["tmp_name"], $upload_barcode_image);
		
		$p_hsn = $_POST['producthsn_'.$i];
		$cgst = $_POST['cgst_'.$i];
		$sgst = $_POST['sgst_'.$i];
		
		$mrpprice = $_POST['mrpprice_'.$i];
		$pricetostockist = $_POST['pricetostockist_'.$i];
		$pricetoretailer = $_POST['pricetoretailer_'.$i];
		
		
		$value = '';
		if($file!='')
		{
			$value .= ", productimage ='".$file."'";
		}
		if($barcode_file!='')
		{
			$value .= ", productbarcodeimage ='".$barcode_file."'";
		}
		
		 $sql_prod_variant = "UPDATE `tbl_product_variant` 
		 SET variant_1='".$size."',variant_2='".$weight."'
		 ,price='".$_POST['price_'.$i]."',mrpprice='$mrpprice'
			".$value.", producthsn = '".$p_hsn."', cgst = '$cgst', sgst = '$sgst', pricetostockist = '$pricetostockist', pricetoretailer = '$pricetoretailer'
		 WHERE `productid`='$id' AND variant_cnt='".$i."'";
		$sql_product_update=mysqli_query($con,$sql_prod_variant);
		
		$commonObj->log_update_record('tbl_product_variant',$id,$sql_prod_variant);
		
		
	}
	$sql_product_check=mysqli_query($con,"select id from `tbl_product` where productname='$productname'");
	
	//if($rowcount = mysqli_num_rows($sql_product_check)>1){	
		//echo "<script>alert('Duplicate Product not allowed.');location.href='product1.php?id='+$id;</script>";
	//}else{
	    //new code aaaaa
		$sql_prod = "UPDATE `tbl_product` SET catid = '$catid' , productname= '$productname'  WHERE id = '$id'";
		$sql_product_update=mysqli_query($con,$sql_prod);
		
		$commonObj->log_update_record('tbl_product',$id,$sql_prod);
		$variant1=fnEncodeString($_POST['variant1']);
		if($variant1!="")
			$var1= implode(',', $variant1);
		
		echo '<script>alert("Product updated successfully.");location.href="product.php";</script>';
	//}
}	
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Product";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Product
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="product.php">Product</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Product</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Product
							</div>
							
						</div>
						<div class="portlet-body"> 
                        <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>						
                        <form id="producteditform" class="form-horizontal" data-parsley-validate=""  role="form" method="post" action="" enctype="multipart/form-data">
					<?php
					$product_id= $_GET['id'];
					$sql_prd="SELECT * from `tbl_product` where id = '$product_id' ";
					$result_prd = mysqli_query($con,$sql_prd);
					if(mysqli_num_rows($result_prd)>0)
					{
					$row_prd = mysqli_fetch_array($result_prd);
					?>          
            
            <div class="form-group">
              <label class="col-md-3">Category:<span class="mandatory">*</span></label>

              <div class="col-md-4">
				<select name="catid"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please select Category"
				class="form-control">
				<option  selected disabled>-Select-</option>
				<?php
				$sql="SELECT c.id as id,b.name, c.categorynm FROM `tbl_category` c,tbl_brand b WHERE b.id=c.brandid";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					$id=$row['id'];
					if($row_prd['catid'] == $id) {
						$sel = "SELECTED";
					}
					else {
						$sel="";
					}
					echo "<option value='$id' $sel>".fnStringToHTML($row['name'])."-" . fnStringToHTML($row['categorynm']) . "</option>";
				} ?>
				</select>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Product Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="productname"
				value="<?php echo fnStringToHTML($row_prd['productname']); ?>"
                placeholder="Enter Product Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter product name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
			 <?php
			$product_id= $_GET['id'];
			$sqlprd="SELECT * from `tbl_product_variant` where productid = '$product_id' ";
			$resultprd = mysqli_query($con,$sqlprd);
			while($rowprd = mysqli_fetch_array($resultprd))
			{
				$exp_variant1 = $rowprd['variant_1'];
				$imp_variant1= split(',',$exp_variant1);
				$exp_variant2 = $rowprd['variant_2'];
				$imp_variant2= split(',',$exp_variant2);
				$prod_var_img = $rowprd['productimage'];
			?>
    
	      <div class="form-group" <?php if($commonObjctype=='1'){
				echo 'style="display: none;"';
			} ?>>	
         
              <label class="col-md-3"><?php echo fnStringToHTML($rowvariant1['name']);?>:</label>

             <div class="col-md-4 nopadl">
			  
			  <div class="col-md-3">
                <input type="text" name="size_<?php echo $rowprd['variant_cnt'];?>" id="numunits1" value="<?php echo fnStringToHTML($imp_variant1[0]); ?>" class="form-control">
              </div>
              
              <div class="col-md-5">
                <select name="cmbUnit_<?php echo $rowprd['variant_cnt'];?>" id="units_variant_id1"  class="form-control">
				<?php 
				$sql_id="SELECT  * FROM `tbl_units_variant` WHERE variantid =40";
				$result_id = mysqli_query($con,$sql_id);
				while($row_id = mysqli_fetch_array($result_id))
				{
					$unitname=$row_id['unitname'];
					$sql="SELECT  unitname,id FROM `tbl_units` WHERE id='$unitname'";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result))
					{
						$unit=$row['id'];
						if($imp_variant1[1] == $unit) {
							$sel = "SELECTED";
						}
						else{
							$sel = "";
						}
						echo "<option value='$unit' $sel>" . fnStringToHTML($row['unitname']) . "</option>";
					}
				} ?>
                </select>
              </div>
				<div class="clearfix"></div> 
              </div>
            </div><!-- /.form-group -->

			<div class="form-group">	
            
              <label class="col-md-3"><?php echo fnStringToHTML($rowvariant2['name']);?>:</label>
		
				<div class="col-md-4 nopadl">

              <div class="col-md-4 " <?php if($commonObjctype=='1'){
				echo 'style="display: none;"';
			} ?>>
                <input type="text" name="weight_<?php echo $rowprd['variant_cnt'];?>" id="numunits2" value="<?php echo fnStringToHTML($imp_variant2[0]); ?>" class="form-control">
              </div>
              <div class="col-md-4">
			  
                <select name="cmbweight_<?php echo $rowprd['variant_cnt'];?>" id="units_variant_id2" class="form-control">
					<?php
					
					$sql_id="SELECT  * FROM `tbl_units_variant` WHERE variantid =41";
					$result_id = mysqli_query($con,$sql_id);
					while($row_id = mysqli_fetch_array($result_id))
					{
						$unitname=$row_id['unitname'];
						$sql="SELECT  unitname,id FROM `tbl_units` WHERE id='$unitname'";
						$result = mysqli_query($con,$sql);
						while($row = mysqli_fetch_array($result))
						{
							$unit=$row['id'];
							if($imp_variant2[1] == $unit) {
								$sel = "SELECTED";
							}
							else {
								$sel = "";
							}
							echo "<option value='$unit' $sel>" . fnStringToHTML($row['unitname']) . "</option>";
						}
					}
					 
					?>
                </select>
			</div>
			</div>
            </div><!-- /.form-group -->	
			<div class="form-group">
              <label class="col-md-3">Product HSN:</label>
              <div class="col-md-4">
                <input type="text" name="producthsn_<?php echo $rowprd['variant_cnt'];?>"
                placeholder="Enter Product HSN"
                data-parsley-trigger="change"	
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control" value="<?=$rowprd['producthsn'];?>" >
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Price:</label>
              <div class="col-md-4">
                <input type="text" name="price_<?php echo $rowprd['variant_cnt'];?>"  placeholder="Price" value="<?php echo fnStringToHTML($rowprd['price']);?>" class="form-control price">
              </div>
            </div><!-- /.form-group -->	
			<div class="form-group">
              <label class="col-md-3">MRP Price:</label>
              <div class="col-md-4">
                <input type="text" name="mrpprice_<?php echo $rowprd['variant_cnt'];?>"  placeholder="MRP Price" value="<?php echo fnStringToHTML($rowprd['mrpprice']);?>" class="form-control price">
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Price To Stockist:</label>
              <div class="col-md-4">
                <input type="text" name="pricetostockist_<?php echo $rowprd['variant_cnt'];?>"  placeholder="Price To Stockist" value="<?php echo fnStringToHTML($rowprd['pricetostockist']);?>" class="form-control price">
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Price To Retailer:</label>
              <div class="col-md-4">
                <input type="text" name="pricetoretailer_<?php echo $rowprd['variant_cnt'];?>"  placeholder="Price To Retailer" value="<?php echo fnStringToHTML($rowprd['pricetoretailer']);?>" class="form-control price">
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">CGST (%):</label>
              <div class="col-md-4">
                <input type="text" name="cgst_<?php echo $rowprd['variant_cnt'];?>" id="cgst[]" placeholder="CGST" value="<?=$rowprd['cgst'];?>" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
              </div>
            </div><!-- /.form-group -->	
			<div class="form-group">
              <label class="col-md-3">SGST (%):</label>
              <div class="col-md-4">
                <input type="text" name="sgst_<?php echo $rowprd['variant_cnt'];?>" id="sgst[]" placeholder="SGST" value="<?=$rowprd['sgst'];?>" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
              </div>
            </div><!-- /.form-group -->	
			
			<div class="form-group">
				  <label class="col-md-3">Product Image:</label>
				  <div class="col-md-4">
				  <?php 
				  if(prod_var_img!=""){?>
				  <img src="upload/<?php echo $prod_var_img;?>" alt="<?php echo $prod_var_img;?>" class="img-responsive"/> 
				  <?php }?>
				  </br>
					 <input type="file" id="productimage" 
					 data-parsley-trigger="change"				
					 data-parsley-fileextension='png,jpg,jpeg' 
					 data-parsley-max-file-size="1000"  
					 name="productimage[]" multiple="multiple">
				  </div>
				</div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Product Barcode Image:</label>
              <div class="col-md-4">
				 <?php 
				  if($rowprd['productbarcodeimage']!=""){?>
				  <img src="upload/<?=$rowprd['productbarcodeimage'];?>" alt="<?=$rowprd['productbarcodeimage'];?>" class="img-responsive"/> 
				  <?php }?>
				  </br>
                <input type="file" id="productbarcodeimage" 
				data-parsley-trigger="change"				
				data-parsley-fileextension='png,jpeg,jpg' 
				data-parsley-max-file-size="1000"  
				name="productbarcodeimage_<?php echo $rowprd['variant_cnt'];?>" multiple="multiple">
              </div>
            </div><!-- /.form-group -->
		<?php }}?> 
                   
            <hr/>      
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="product.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group --> 
          </form>  
                            
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<script>
/*$('#producteditform').submit(function () {
	var inputstring1="";var inputstring2="";var temp=0;var flag=0;
	$( "input[name^='price_']").each(function(i){
		temp=i+1;
		inputstring1=$("input[name=size_"+temp+"]").val();
		inputstring2=$("input[name=weight_"+temp+"]").val();
		if(inputstring1==''&& inputstring2==''){
			flag=flag+1;
			switch(temp){
				case 1:
					alert("Atleast one variant to be required either Pcs or Weight in first!");
					return false
					break;
				case 2:alert("Atleast one variant to be required either Pcs or Weight for second!");
					return false
					break;
				case 3:alert("Atleast one variant to be required either Pcs or Weight for third!");
					return false
					break;
				default:alert("Atleast one variant to be required either Pcs or Weight for "+temp+"th !");
					return false
					break;
			}
		}
	})
	if(flag==0){
		alert("trjsdhfj");
		return true;
	}else{
		alert("faldfsh");
		return false;
	}
});*/

$(document).ready(function() {
    window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
            // the value contains the file path, so we can pop the extension
			var arrRequirement = requirement.split(',');			
            var fileExtension = value.split('.').pop();
			var result = jQuery.inArray( fileExtension, arrRequirement );
			 
			if(result==-1)
            return false;
			else 
			return true;
        }, 32)
        .addMessage('en', 'fileextension', 'Only png or jpg image are allowed');	
window.Parsley.addValidator('maxFileSize', {
  validateString: function(_value, maxSize, parsleyInstance) {
    var files = parsleyInstance.$element[0].files;
    return files.length != 1  || files[0].size <= maxSize * 1024;
  },
  requirementType: 'integer',
  messages: {
    en: 'Image should not be larger than %s Kb',
  }
});
	
	$( "input[name^='price']").keyup(function (e) {
            if (this.value != "") {
                var arrtmp = this.value.split(".");
                if (arrtmp.length > 1) {
                    var strTmp = arrtmp[1];
                    if (strTmp.length > 2) {
                        this.value = this.value.substring(0, this.value.length - 1);
                    }
                }
            }
        }); 
		$( "input[name^='mrpprice_']").keyup(function (e) {
            if (this.value != "") {
                var arrtmp = this.value.split(".");
                if (arrtmp.length > 1) {
                    var strTmp = arrtmp[1];
                    if (strTmp.length > 2) {
                        this.value = this.value.substring(0, this.value.length - 1);
                    }
                }
            }
        }); 
		
		$( "input[name^='cgst_']").keyup(function (e) {
            if (this.value != "") {
                var arrtmp = this.value.split(".");
                if (arrtmp.length > 1) {
                    var strTmp = arrtmp[1];
                    if (strTmp.length > 2) {
                        this.value = this.value.substring(0, this.value.length - 1);
                    }
                }
            }
        });
		$( "input[name^='sgst_']").keyup(function (e) {
            if (this.value != "") {
                var arrtmp = this.value.split(".");
                if (arrtmp.length > 1) {
                    var strTmp = arrtmp[1];
                    if (strTmp.length > 2) {
                        this.value = this.value.substring(0, this.value.length - 1);
                    }
                }
            }
        });
});
</script>
</body>
<!-- END BODY -->
</html>