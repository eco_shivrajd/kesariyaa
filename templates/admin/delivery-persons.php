<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
//$userObj->migration_sp_sstockist(); exit;

?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "DeliveryPerson";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Delivery Person
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Delivery Person</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Delivery Persons Listing
							</div>
                            <a href="delivery-person-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Delivery Person
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th width="15%">
									 Name
								</th>	
								<!--<th width="25%">
									Assigned Region
								</th>	-->							
                                <th width="20%">
									 Email
								</th>
                                <th width="10%">
									 Mobile Number
								</th>
								<th width="10%">
                                	City
                                </th> 
								<th width="10%">
                                	Region
                                </th>    
								<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ ?>
								<th width="10%">
                                	Action
                                </th>
								<?php } ?>
							</tr>
							</thead>
							<tbody>
							<?php
							switch($_SESSION[SESSION_PREFIX.'user_type']){
								case "Admin":									
									$user_type="SalesPerson";									
									$external_id = "";
									break;
								case "Superstockist":
									$user_type="SalesPerson";		
									$external_id = $_SESSION[SESSION_PREFIX.'user_id'];									
								break;
								case "Distributor":								
									$user_type="SalesPerson";									
									$external_id = $_SESSION[SESSION_PREFIX.'user_id'];
								break;
							}
							$user_type='DeliveryPerson';
							$result1 = $userObj->getAllLocalUserDetails($user_type,$external_id);
							//echo "<pre>";print_r($result1);die();
						
							while($row = mysqli_fetch_array($result1))
							{ $user_id=$row['id']; 
								$working_area_details = $userObj->getLocalUserWorkingAreaDetails($row['id']);
								if($working_area_details == 0){	
									//echo "<pre>";print_r($working_area_details);
									$row['state_ids'] = $row['state'];									
									$row['city_ids'] = $row['city'];
									$row['suburb_ids'] = $row['suburb_ids'];
								}elseif(count($working_area_details) > 0){	
									$row = array_merge($row,$working_area_details);
								}
						?>
							<tr class="odd gradeX">
								<td>
									 <a href="delivery-person-edit.php?id=<?php echo $user_id;?>"><?php echo fnStringToHTML($row['firstname']);?></a>
								</td>
								<td><?php echo $row['email']?></td>
                                <td><?php echo $row['mobile']?></td>
								<td><?php 
								$city_id=$row['city_ids'];
								if(!empty($city_id)){
									$sql="SELECT * FROM tbl_city where id = $city_id";
									$result = mysqli_query($con,$sql);
									while($num = mysqli_fetch_array($result))
									{ 
										echo  $num['name'];
									} 
								}else{
									echo '-';
								}
								?>
								</td>
								<td><?php 
								$area_id=$row['suburb_ids'];
								//echo $area_id;
								if(!empty($area_id)){
									$sql="SELECT * FROM tbl_surb where id = $area_id";
									$result = mysqli_query($con,$sql);
									while($num = mysqli_fetch_array($result))
									{ 
										echo  $num['suburbnm'];
									} 
								}else{
									echo '-';
								}
								?></td>
								
								<td><a href="manageuser.php?utype=DeliveryPerson&id=<?php echo $row['id']?>">Delete</a></td>
							</tr>
							<?php } ?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->

</body>
<!-- END BODY -->
</html>