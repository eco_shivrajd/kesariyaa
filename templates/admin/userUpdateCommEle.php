<?php
	//$user_level_readonly = 'readonly disabled';
	/*if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
	{
		if($page_to_update == 'sales_person' && $_SESSION[SESSION_PREFIX.'user_type']=="Superstockist")
			$user_level_readonly = '';
		else
			$user_level_readonly = 'readonly disabled';
	}else if($page_to_update == 'superstockist' && $_SESSION[SESSION_PREFIX.'user_type']=="Admin")
	{
		$user_level_readonly = 'readonly disabled';
	}*/
	
		
?>

 <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"				
				name="firstname" class="form-control" value="<?php echo fnStringToHTML($row1['firstname'])?>"
				<?php if($page_to_update == 'sales_person' && $_SESSION[SESSION_PREFIX."user_type"]=="Distributor") { echo "disabled"; }?> >
              </div>
            </div><!-- /.form-group -->
             
			<div class="form-group">
              <label class="col-md-3">Username:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" id="username"
				placeholder="Enter Username"
				data-parsley-minlength="6"
				data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces"          
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"          
				data-parsley-type-message="Please enter Username, blank spaces are not allowed"		   
				data-parsley-required-message="Please enter Username, blank spaces are not allowed"
				data-parsley-trigger="change"
				data-parsley-required="true"
				data-parsley-pattern="/^\S*$/" 
				data-parsley-error-message="Username should be 6-50 characters without blank spaces"
				name="username" class="form-control" 
				value="<?php echo fnStringToHTML($row1['username'])?>"
				<?php if($page_to_update == 'sales_person' && $_SESSION[SESSION_PREFIX."user_type"]=="Distributor") { echo "disabled"; }?>><span id="user-availability-status"></span>
              </div>
            </div><!-- /.form-group -->
			
 
<!--<div class="form-group">
  <label class="col-md-3">Password:</label>
  <div class="col-md-4">
	<input type="text" id="password" name="password" class="form-control" readonly disabled value="<?php echo fnStringToHTML($row1['pwd'])?>"><span id="user-availability-status"></span>
  </div>
</div>--><!-- /.form-group -->
 <div class="form-group">
			  <label class="col-md-3">Address:<span class="mandatory">*</span></label>

			  <div class="col-md-4">
				<textarea name="address"
				 placeholder="Enter Address"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"							
				rows="4" class="form-control" <?php if($page_to_update == 'sales_person' && $_SESSION[SESSION_PREFIX."user_type"]=="Distributor") { echo "disabled"; }?>><?php echo fnStringToHTML($row1['address'])?></textarea>
			  </div>
			</div><!-- /.form-group -->
			<?php if($page_to_update == 'delivery_person'||$page_to_update == 'stockist'){ ?>
			<div id="working_area_top">		
			<h4>Assign <?php if($page_to_update == 'stockist'){?>Area<?php }else{ ?>City<?php }?></h4>		
			</div>
			<div class="form-group">
			  <label class="col-md-3">State:<span class="mandatory">*</span></label>
			  <div class="col-md-4">
				<?php
					$sql_state="SELECT * FROM tbl_state where country_id=101";
					
					$result_state = mysqli_query($con,$sql_state);		
				?>
				<select name="state" id="state" class="form-control" 
				data-parsley-trigger="change"
				data-parsley-required="#true" 				
				data-parsley-required-message="Please select State"
				parsley-required="true"
				data-parsley-required-message="Please select State"
				 onChange="fnShowCity(this)">
					<?php
						if($row1['state_ids'] == 0)
							echo "<option value=''>-Select-</option>";
						while($row_state = mysqli_fetch_array($result_state))
						{
							$selected = "";
							if($row_state['id'] == $row1['state_ids'])
								$selected = "selected";				
						
							echo "<option value='".$row_state['id']."' $selected>" . fnStringToHTML($row_state['name']) . "</option>";
						}
					?>
				</select>
			  </div>
			</div><!-- /.form-group -->

			<div class="form-group" id="city_div">
			  <label class="col-md-3">City:<span class="mandatory">*</span></label>
			  <div class="col-md-4" id="div_select_city">
				<?php
					$option = "<option value=''>-Select-</option>";
					if($row1['city_ids'] != '')
					{
						$sql_city="SELECT * FROM tbl_city where state_id='".$row1['state_ids']."' ORDER BY name";
						
						$result_city = mysqli_query($con,$sql_city);
						while($row_city = mysqli_fetch_array($result_city)){
							$cat_id=$row_city['id'];
							$selected = "";
							if($row_city['id'] == $row1['city_ids'])
								$selected = "selected";				
							
							$option.= "<option value='$cat_id' $selected>".$row_city['name']."</option>";
						}
					}												
				?>
				<select name="city" id="city" class="form-control"  onchange="FnGetSuburbDropDown(this)" 
				data-parsley-trigger="change"
				data-parsley-required="#true"	
				data-parsley-required-message="Please select City"
				><?=$option;?></select>
			  </div>
			</div><!-- /.form-group --> 
			<?php if($page_to_update != 'superstockist'){ ?>
			<div class="form-group">
			  <label class="col-md-3">Area:</label>
			<?php
				if($row1['suburb_ids'] != '')
					$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_surb WHERE id IN (".$row1['suburb_ids'].")";
				else
					$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_surb WHERE cityid = '".$row1['city_ids'] . "'";
				
				$result_area = mysqli_query($con,$sql_area);
				$suburb_array = explode(',', $row1['suburb_ids']);
				$multiple = '';
				if(mysqli_num_rows($result_area) > 1)
					$multiple = 'multiple';
				
				$selected = "";
			?>
			  <div class="col-md-4"  id="div_select_area">
			  <select name="area[]" id="area" class="form-control" <?=$multiple;?> onchange="FnGetSubareaDropDown(this)">	
				<option value="">-Select-</option>
				<?php
				while($row_area = mysqli_fetch_array($result_area))
				{				
					if(count($suburb_array) > 0)
					{
						if(in_array($row_area['id'], $suburb_array))
							$selected = "selected";
						
					}
					echo "<option value='".$row_area['id']."' $selected>" . fnStringToHTML($row_area['suburbnm']) . "</option>";
				}
				?>
				</select>				
			  </div>
			</div>
			<input type="hidden" id="selected_taluka" name="selected_taluka" value="<?=$suburb_array[0];?>">
			<!-- /.form-group -->
			<?php if($page_to_update != 'stockist' && $page_to_update != 'delivery_person'){ ?>
			<div class="form-group" id="subarea_div">
			  <label class="col-md-3">Subarea:</label>
			  <?php
				$rows_subarea = 0;
				if(isset($row1['subarea_ids']) && $row1['subarea_ids'] != '')
				{
					$sql_subarea="SELECT `subarea_id`, `state_id`, `city_id`, `suburb_id`, `subarea_name` 
					FROM tbl_subarea WHERE subarea_id IN (".$row1['subarea_ids'].")";						
					
					$result_subarea = mysqli_query($con,$sql_subarea);
					if($result_subarea != '')
						$rows_subarea = mysqli_num_rows($result_subarea);
					$subarea_array = explode(',', $row1['subarea_ids']);
					$multiple = '';
					if(count($subarea_array) > 1)
						$multiple = 'multiple';
					
					$selected = '';
				}
			  ?>
			  <div class="col-md-4" id="div_select_subarea">
			  <select name="subarea[]" id="subarea" data-parsley-trigger="change" class="form-control" <?=$multiple;?>>					
					<?php		
					if($rows_subarea == 0){ 
						echo '<option value="">-Select-</option>';
					}else{
						while($row_subarea = mysqli_fetch_array($result_subarea))
						{		
							if(count($subarea_array) > 0)
							{
								if(in_array($row_subarea['subarea_id'], $subarea_array))
									$selected = "selected";
								
							}
							echo "<option value='".$row_subarea['subarea_id']."' $selected>" . fnStringToHTML($row_subarea['subarea_name']) . "</option>";
						}
					}
					?>
				</select>
			  </div>
			</div><!-- /.form-group --> 
			<?php } } ?>
<div id="working_area_bottom">				
</div>
			<?php } ?>
<div class="form-group">
			  <label class="col-md-3">Email:</label>

			  <div class="col-md-4">
				<input type="text" name="email" id="email" 
				placeholder="Enter E-mail"
				data-parsley-maxlength="100"
				data-parsley-maxlength-message="Only 100 characters are allowed"
				data-parsley-type="email"
				data-parsley-type-message="Please enter valid e-mail"		   
				
				data-parsley-trigger="change"
				
				class="form-control" value="<?php echo fnStringToHTML($row1['email'])?>"
				<?php if($page_to_update == 'sales_person' && $_SESSION[SESSION_PREFIX."user_type"]=="Distributor") { echo "disabled"; }?>>
			  </div>
			</div><!-- /.form-group -->


			<div class="form-group">
			  <label class="col-md-3">Mobile Number:<span class="mandatory">*</span></label>

			  <div class="col-md-4">
				<input type="text"  name="mobile"
				placeholder="Enter Mobile Number"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number"
				data-parsley-minlength="10"
				data-parsley-minlength-message="Mobile number should be of minimum 10 digits" 
				data-parsley-maxlength="15"				    
				data-parsley-maxlength-message="Only 15 digits are allowed"
				data-parsley-pattern="^(?!\s)[0-9]*$"
				data-parsley-pattern-message="Please enter numbers only"
				class="form-control" value="<?php echo fnStringToHTML($row1['mobile'])?>"
				<?php if($page_to_update == 'sales_person' && $_SESSION[SESSION_PREFIX."user_type"]=="Distributor") { echo "disabled"; }?>>
			  </div>
			</div><!-- /.form-group -->
			<?php if($page_to_update == 'superstockist'||$page_to_update =='stockist'){ ?>
				<div class="form-group">
              <label class="col-md-3">GST Number:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter GST Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter GST Number"
				data-parsley-maxlength="15"
				data-parsley-maxlength-message="Only 15 characters are allowed"	
				data-parsley-minlength="15"
				data-parsley-minlength-message="Not Valid GST Number"	
				name="gstnumber" class="form-control" value="<?php echo fnStringToHTML($row1['gst_number_sss'])?>">
              </div>
            </div><!-- /.form-group -->
				<?php } ?>
            <input type="hidden" name="page_to_update" id="page_to_update" value="<?=$page_to_update;?>">
<script>
function fnShowCity(id) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');
	var page_to_update = $("#page_to_update").val();
	var param = '';
	if(page_to_update == 'superstockist')
		param = "&nofunction=nofunction";
	var url = "getCityDropDown.php?cat_id="+id.value+"&select_name_id=city&mandatory=mandatory"+param;
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');
	var page_to_update = $("#page_to_update").val();
	var param = '';
	if(page_to_update == 'stockist')
		param = "&nofunction=nofunction";	
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown"+param;
	CallAJAX(url,"div_select_area");
}

 

function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function FnGetSubareaDropDown(id) {
	var suburb_str = $("#area").val();	
	$("#subarea").html('<option value="">-Select-</option>');	
	if(suburb_str != null)	{
		var suburb_arr_count = calculate_data_count(suburb_str);
		if(suburb_arr_count == 1){//If single city selected then only show its related subarea	
			$("#subarea_div").show();	
			var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
			CallAJAX(url,"div_select_subarea");
		}else if(suburb_arr_count > 1){
			$("#subarea_div").show();	
			var multiple_id = suburb_str.join(", ");
			var url = "getSubareaDropdown.php?multiple_id="+multiple_id+"&select_name_id=subarea&multiple=multiple";
			CallAJAX(url,"div_select_subarea");
		}else{
			setSelectNoValue("div_select_subarea", "subarea");
		}	
	}else{
			setSelectNoValue("div_select_subarea", "subarea");
		}	
}

function checkAvailability() {
	$('#updateform').parsley().validate();
	var email = $("#email").val();
	var username = $("#username").val();
	var id = $("#id").val();
	if(username != '')
	{
		jQuery.ajax({
			url: "../includes/checkUserAvailable.php",
			data:'validation_field=username&username='+username+'&id='+id,
			type: "POST",
			async:false,
			success:function(data){		
				if(data=="exist") {
					alert('Username already exists.');					
					return false;
				} else {
					if(email != '')
					{
						jQuery.ajax({
							url: "../includes/checkUserAvailable.php",
							data:'validation_field=email&email='+email+'&id='+id,
							type: "POST",
							async:false,
							success:function(data){			
								if(data=="exist") {
									alert('E-mail already exists.');									
									return false;
								} else {	
									submitFrm();
								}
							},
							error:function (){}
						});
					} else {
						submitFrm();
					}									
				}
			},
			error:function (){}
		});
	}
}
/*
function checkAvailability() {
	var email = $("#email").val();	
	var id = $("#id").val();
	var validate = 0;	
	 
	if(validate == 0)
	{
		var action = $('#hidAction').val();
		$('#updateform').attr('action', action);					
		$('#hidbtnsubmit').val("submit");
		$('#updateform').submit();
	}	
}*/

function submitFrm(){
	var action = $('#hidAction').val();
	$('#updateform').attr('action', action);					
	$('#hidbtnsubmit').val("submit");
	$('#updateform').submit();
}
</script> 