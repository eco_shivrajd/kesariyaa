<?php
include ("../../includes/config.php");
extract($_POST);
$sql = "SELECT
	VO.id, shops.name as shopname,
	shops.address,VO.shopid as shopidtemp, 
	shops.contact_person shopcontact,
	shops.mobile as shopmobile,
	salesman.firstname,
	salesman.mobile, 
	OA.order_date , 
	OA.categorynm,  
	VO.orderid, 
	VO.variantweight , 
	VO.weightquantity,
	VO.unit, 
	VO.variantunit, 
	VO.totalcost , VO.campaign_type, VO.campaign_applied, TP.productname,VO.campaign_sale_type
FROM tbl_order_app OA 
LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid
 LEFT JOIN tbl_product TP ON VO.productid = TP.id
LEFT JOIN tbl_shops shops ON shops.id= VO.shopid
LEFT JOIN tbl_user salesman ON salesman.id= OA.order_by 

WHERE
	date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' ";//aaa shopcontact,shopmobile

$condition = "";

switch($_SESSION[SESSION_PREFIX.'user_type']) {
	case "Admin":
	 
	break;
	case "Superstockist":													
		$condition .= "  AND OA.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ";
	break;
	case "Distributor":													
		$condition .= "  AND OA.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ";
	break;
}

if($dropdownSalesPerson!="")
{
	$condition .= " AND OA.order_by = " . $dropdownSalesPerson;
} else if( $dropdownStockist!="") {
	$condition .= " AND OA.distributorid = " . $dropdownStockist;
} else if($cmbSuperStockist!="") {
	$condition .= " AND OA.superstockistid = " . $cmbSuperStockist;
}

if($dropdownbrands  !="") {
	$condition .= " AND OA.brandid = " . $dropdownbrands;
}

if($dropdownCategory  !="") {
	$condition .= " AND OA.catid = " . $dropdownCategory;
}
if($dropdownshops  !="") {
	$condition .= " AND VO.shopid = " . $dropdownshops;
}
if($dropdownProducts  !="") {
	$condition .= " AND VO.productid = " . $dropdownProducts;
}

if($subarea !="") {
	$condition .= " AND shops.subarea_id = " . $subarea;
}
if($dropdownSuburbs !="") {
	$condition .= " AND shops.suburbid = " . $dropdownSuburbs;
}

if($dropdownCity !="") {
	$condition .= " AND shops.city = " . $dropdownCity;
}
if($dropdownState !="") {
	$condition .= " AND shops.state = " . $dropdownState;
}

$sql .= $condition;
$sql .= " order by VO.shopid";
//echo $sql;
$result1 = mysqli_query($con,$sql); 
//$resarr=mysqli_fetch_array($result1);
$totalRecords=mysqli_num_rows($result1);

//$allshopids=array();
//while($rowcount = mysqli_fetch_array($result1)) {
//	$allshopids[] = $rowcount["shopidtemp"];
//}
//$allshopids=array_count_values($allshopids);
?>
<? if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<? if($_GET["actionType"]!="excel") { ?>
		<div class="caption"><i class="icon-puzzle"></i>Daily Sales Report</div>
		<?  if($totalRecords > 0) { ?>
			<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
			&nbsp;
			<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		
		<? } } ?>
	</div>
	
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<table class="table table-striped table-hover table-bordered responsive">
				<thead>
					<tr>
						<th>Date & Time</th>
						<th>Shop name </th>
						<th>Product</th>
						<th>Weight</th>
						<th>Quantity</th>
						<th>Total Price</th>
					</tr>
				</thead>
				<tbody>
				<? $newarr=array();$newarr1=array();
				while($row = mysqli_fetch_array($result1)) {
					$shopnm = $row["shopname"];
					if($row["address"]!="")
						//$shopnm .= "<br>" . $row["address"];				
					$ContactPerson = fnStringToHTML($row["shopcontact"]);//firstname
					
					if($row["shopmobile"]!="")
						$ContactPerson .= "<br>" . $row["shopmobile"];
					
					$Quantity = $row['variantunit'];
					
					$TotalCost = $row['totalcost'];
					$total_cost = $Quantity * $TotalCost;
			
					$total_cost = number_format($total_cost,2, '.', '');
						
					
					$OtherDetails = "Quantity: " . $Quantity. ", Total Price: " . $total_cost;
					
					$display_icon = '';
					if($row['campaign_sale_type'] == 'free')
						$display_icon = '&nbsp;&nbsp;<span><img src="'.SITEURL.'/assets/global/img/free-icon.png" title="Free Product"></span>';											
				
				$newarr['order_date']=date('d-m-Y H:i:s',strtotime($row["order_date"]));
				$newarr['ids']=$row["shopidtemp"];				
				$newarr['shopnm']=$shopnm;
				$newarr['ContactPerson']=$ContactPerson;
				//$newarr['categorynm']=$row["categorynm"];
				$newarr['productname']=$row["productname"];
				$newarr['weightquantity']=$row["weightquantity"].'-'.$row["unit"];
				$newarr['Quantity']=$Quantity.''.$display_icon;
				$newarr['total_cost']=$total_cost;
				$newarr1[]=$newarr;
				 } //echo "<pre>";print_r($newarr1);
				$out = array();
				foreach ($newarr1 as $key => $value){
					$countval=0;
					foreach ($value as $key2 => $value2){
						if($key2=='ids'){
							 $index = $key2.'-'.$value2;
							if (array_key_exists($index, $out)){
								$out[$index]++;
							} else {
								$out[$index] = 1;
							}
						}
						
					}
				}
				$out1 = array_values($out);$j=0;$temp=$newarr1[0]["shopnm"];$sum=0;
				$gtotalq=0;$gtotalp=0;
				for($i=0;$i<count($newarr1);$i++){
				?>
					<tr>
					<? 
					if($temp==$newarr1[$i]["shopnm"]){						
						$totalq=0;$totalp=0;
							?>	
					<td rowspan='<?=$out1[$j];?>'><?=date('d-m-Y H:i:s',strtotime($newarr1[$i]["order_date"]));?></td>
						<td rowspan='<?=$out1[$j];?>'><?=$newarr1[$i]["shopnm"];?></td>
						<? $sum=$sum+$out1[$j]; $temp=$newarr1[$sum]["shopnm"]; $j++;  } ?>
						<td><?=$newarr1[$i]["productname"];?></td>
						<td><?=$newarr1[$i]["weightquantity"];?></td>						
						<td><?=$newarr1[$i]["Quantity"];?></td>
						<td><?=$newarr1[$i]["total_cost"];?></td>
						<!--". $row["orderid"]. "-->
					</tr>
					<? 
					$totalq=$totalq+$newarr1[$i]["Quantity"];
					$totalp=$totalp+$newarr1[$i]["total_cost"];
					if($i==($sum-1)){ 
					$gtotalq=$gtotalq+$totalq;
					$gtotalp=$gtotalp+$totalp;
					?>	
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><b>Total</b></td>
						<td><b><?= $totalq;?></b></td>
						<td><b><?= $totalp;?></b></td>
					</tr>
						<?  } ?>	
					<?
				}
				//echo"<pre>";print_r($out1);
				?>
				<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><b>Grand Total</b></td>
						<td><b><?= $gtotalq;?></b></td>
						<td><b><?= $gtotalp;?></b></td>
					</tr>
				 </tbody>
			</table>
		</div>
	</div>
</div>
<?
if($_GET["actionType"]=="excel") {
	header("Content-Type: application/vnd.ms-excel");
	header("Content-disposition: attachment; filename=Report_".$frmdate.".xls");
} ?>
 