<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";	
$userObj 	= 	new userManager($con,$conmain);
if(isset($_POST['hidbtnsubmit']))
{	
	
	//print"<pre>";print_r($_POST);die();
	$user_type="DeliveryPerson";	
	
	$userid_local = $userObj->addLocalUserDetails($user_type);
	$userObj->addLocalUserWorkingAreaDetails($userid_local);
	
	$username	=	fnEncodeString($_POST['username']);
	$common_user_record = $userObj->getCommonUserDetailsByUsername($username);	
	if($common_user_record != 0)
	{
		$common_user_company_record = $userObj->getCommonUserCompanyDetails($userid_local);
		if($common_user_company_record != 0)
		{
			$userObj-addCommonUserCompanyDetails($userid_local);
		}
	}else{
		$userid_common = $userObj->addCommonUserDetails($user_type,$userid_local);
		$userObj->addCommonUserCompanyDetails($userid_local);
	}	
	$email		=	fnEncodeString($_POST['email']);
	if($email != '')
		$userObj->sendUserCreationEmail();
	
	echo '<script>alert("Delivery person added successfully.");location.href="delivery-persons.php";</script>';
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<?php
$activeMainMenu = "ManageSupplyChain"; $activeMenu = "DeliveryPerson";
include "../includes/sidebar.php";
?>

<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">	 
		<h3 class="page-title">Delivery Person</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				 
				<li>
					<i class="fa fa-home"></i>
					<a href="delivery-persons.php">Delivery Person</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Add New Delivery Person</a>
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet box blue-steel">
				<div class="portlet-title"><div class="caption">Add New Delivery Person</div></div>
				<div class="portlet-body">
					<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
					<form name="addform" id="addform" class="form-horizontal" role="form" data-parsley-validate=""  method="post" action="">          
						
						<?php $page_to_add = 'delivery_person';  include "userAddCommEle.php";	//form common element file with javascript validation ?>    
						<div class="form-group">
						  <label class="col-md-3">Office Phone No.:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Office Phone No."
							data-parsley-trigger="change"					
							data-parsley-minlength="10"
							data-parsley-maxlength="15"
							data-parsley-maxlength-message="Only 15 characters are allowed"
							data-parsley-pattern="^(?!\s)[0-9]*$"
							data-parsley-pattern-message="Please enter numbers only"
							name="officephone" class="form-control">
						  </div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name <span class="mandatory">*</span></label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"				
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter accouont number"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="accholdername" class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter accouont number"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="accno" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter branch Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="accbrnm" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter ifsc Code"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="accifsc" class="form-control">
						  </div>
						</div>
						<!--<div class="form-group">
							<label class="col-md-3">Status</label>
							<div class="col-md-4">                                                   
								<div class="radio-list">
								<label class="radio-inline">
								<input type="radio" name="optionsRadios" id="optionsRadios4" value="option1"> Active </label>
								<label class="radio-inline">
								<input type="radio" name="optionsRadios" id="optionsRadios5" value="option2"> Inactive </label>
								</div>
							</div>
						</div>-->
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
							<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
							<input type="hidden" name="hidAction" id="hidAction" value="delivery-person-add.php">
							<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
							<a href="delivery-persons.php" class="btn btn-primary">Cancel</a>
							</div>
						</div><!-- /.form-group -->
					</form>  
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
	<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<style>
.form-horizontal { font-weight:normal; }
</style>
</body>
<!-- END BODY -->
</html>
<script>
function fnShowStockist(id){
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","fetchstockist.php?cat_id="+id.value+"&multiple=multiple&user_type_manage=sales",true);
	xmlhttp.send();
}
</script>