<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") {
	header("location:../logout.php");
} ?>
<!-- END HEADER -->
<?php

if(isset($_POST['hidbtnsubmit'])) 
{
	$id=$_POST['id'];
	$user_type = "Distributor";
	switch($_SESSION[SESSION_PREFIX.'user_type']){
	
		case "Admin":
		case "Superstockist":
			$userObj->updateLocalUserDetails($user_type, $id);
			$userObj->updateCommonUserDetails($id);
			$working_detail = $userObj->getLocalUserWorkingAreaDetails($id);
			if($working_detail == 0)
				$userObj->addLocalUserWorkingAreaDetails($id);
			else
				$userObj->updateLocalUserWorkingAreaDetails($id);	
			echo '<script>location.href="distributors.php";</script>';
		break;
		case "Distributor": break;
	}
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Stockist";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Stockist
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="distributors.php">Stockist</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
					<a href="#"><? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { echo "Edit Stockist"; } else { echo "Stockist Details"; } ?></a> 
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								 <? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
								Edit Stockist
								<? } else {?>
								Stockist Details
								<? } ?>
							</div>
							<a href="updatepassword.php?id=<?php echo base64_encode($_GET['id']);?>" class="btn btn-sm btn-default pull-right mt5">							
                                Change Password
                              </a>
						</div>
						<div class="portlet-body">
						<? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						<?}?>
					 <?php
					$id=$_GET['id'];
					//$sql1="SELECT * FROM tbl_user where id = '$id' ";
					//$result1 = mysqli_query($con,$sql1);
					$userObj 	= 	new userManager($con,$conmain);
					$user_details = $userObj->getLocalUserDetails($id);
					$working_area_details = $userObj->getLocalUserWorkingAreaDetails($id);
					if($working_area_details != 0)
					{
						$row1 = array_merge($user_details,$working_area_details);//print"<pre>";print_r($row1);
						if($row1['state_ids'] == '')
							$row1['state_ids'] = $user_details['state'];
						if($row1['city_ids'] == '')
							$row1['city_ids'] = $user_details['city'];
					}
					else{
						$row1 = $user_details;
						$row1['state_ids'] = $user_details['state'];
						$row1['city_ids'] = $user_details['city'];
					}
					
					 ?>                       
					<form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" method="post" action="">
					
						<? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
							<div class="form-group" style="display: none">
							  <label class="col-md-3">Superstockist:<span class="mandatory">*</span></label>
							  <div class="col-md-4">
								<select name="assign" class="form-control" onchange="javascript:showStateCity(this);">
									<?php
									$user_type="Superstockist";
									$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' and isdeleted!='1' order by is_default_user desc, firstname asc";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										$assign_id=$row['id'];
										if($row1['external_id'] == $assign_id)
											$sel="SELECTED";
										else
											$sel="";
										echo "<option value='$assign_id' $sel>" . fnStringToHTML($row['firstname']) . "</option>";
									} ?>
								</select>
							  </div>
							</div><!-- /.form-group --> 
							<? } else { ?>
								<input type="hidden" name="assign" id="assign" value="<?=$_SESSION[SESSION_PREFIX.'user_id']?>">
							<? } ?>		
							
							<?php $page_to_update = 'stockist'; include "userUpdateCommEle.php";	//form common element file with javascript validation ?>             
            
				<div class="form-group">
				  <div class="col-md-4 col-md-offset-3">
					<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
					<input type="hidden" name="hidAction" id="hidAction" value="distributor1.php">
					<input type="hidden" name="id" id="id" value="<?=$row1['id'];?>">
					 
					<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
					 
					<a href="distributors.php" class="btn btn-primary">Cancel</a>  
					 
				  </div>
				</div><!-- /.form-group -->

			  </form>
		  
	  	  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
			<div class="modal-dialog" style="width:300px;">
				<div class="modal-content">
					<div class="modal-body">
						<p>
						<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
						</p>                     
					  <center><a href="distributor_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
					  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
					  </center>
					</div>    
				</div>
			</div>
		</div>		  
                          
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<script>
function showStateCity(obj){
	var ss_id = obj.value;
	jQuery.ajax({
			url: "../includes/ajax_getUserAssLocation.php",
			data:'ss_id='+ss_id,
			type: "POST",
			async:false,
			success:function(data){		
				var user_record = JSON.parse(data);
				if(user_record!=0) {
					var state = user_record.state;
					var city = user_record.city;
					$("#state").val(state);					
					setTimeout(
					function() 
					{	
						fnShowCity(state);
						setTimeout(
						function() 
						{
							$("#city").val(city);	
						}, 1800);
					}, 600);
					setTimeout(function() 
					{
						FnGetSuburbDropDown($("#city").get(0));
						setTimeout(
						function() 
						{
							var selected_taluka = $("#selected_taluka").val();					
							$("#area").val(selected_taluka);
						}, 3800);
					}, 2800);					
				} 
			},
			error:function (){}
		});
}
</script>
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>