<?php
include ("../../includes/config.php");
extract($_POST);
$maxDays=date('t');
$StartDate = "01-" . $drpMonthlyOption;
$MonthName = date('F', strtotime($StartDate . ' +0 day'));
$endDate = $maxDays . "-" . $drpMonthlyOption;

function getProductData($con, $counter , $dateToShow , $arrProductInfo,$arrPOST) 
{	
	extract($arrPOST);
	$returnString = "";
	$AllCat_totalquantity = 0;
	$AllCat_totalcost = 0;
	
	for($i=0;$i<$counter;$i++)
	{
		$sql = "SELECT 
			VO.variantunit as unit,
			sum(VO.variantunit) as TotalVariantunit,
			sum(VO.variantunit) as totalquantity,
			sum(VO.totalcost*VO.variantunit) as totalcost
		FROM 
			tbl_order_app OA LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid 
			LEFT JOIN tbl_shops shops ON shops.id= VO.shopid	
		WHERE 		
			date_format(OA.order_date, '%d-%m-%Y') = '".$dateToShow."' ";
		
		$condition = " AND OA.catid = " . $arrProductInfo[$i]['catid'];
		
		
		switch($_SESSION[SESSION_PREFIX.'user_type']) {
			case "Admin":
			 
			break;
			case "Superstockist":													
				$condition .= "  AND OA.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ";
			break;
			case "Distributor":													
				$condition .= "  AND OA.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ";
			break;
		}

		
		if($dropdownSalesPerson!="")
		{
			$condition .= " AND OA.order_by = " . $dropdownSalesPerson;
		} else if( $dropdownStockist!="") {
			$condition .= " AND OA.distributorid = " . $dropdownStockist;
		} else if($cmbSuperStockist!="") {
			$condition .= " AND OA.superstockistid = " . $cmbSuperStockist;
		}
		/*if($dropdownSalesPerson  !="")
		{
			$condition .= " AND OA.order_by = " . $dropdownSalesPerson;
		}
		
		if($dropdownSalesPerson  =="")
		{
			if($dropdownStockist  !="")
			{
				$condition .= " AND OA.distributorid = " . $dropdownStockist;
			}
		}
		
		if($dropdownStockist  ==""){
			if($cmbSuperStockist !="")
			{
				$condition .= " AND OA.superstockistid = " . $cmbSuperStockist;
			}
		}*/
		
		if($dropdownshops !="")
		{
			$condition .= " AND VO.shopid = " . $dropdownshops;
		}
		
		if($dropdownbrands  !="")
		{
			$condition .= " AND OA.brandid = " . $dropdownbrands;
		}  
		
		if($dropdownProducts  !="")
		{
			$condition .= " AND VO.productid = " . $dropdownProducts;
		}
		
		if($subarea !="") {
			$condition .= " AND shops.subarea_id = " . $subarea;
		}
		
		if($dropdownSuburbs !="")
		{
			$condition .= " AND shops.suburbid = " . $dropdownSuburbs;
		}
		
		if($dropdownCity !="")
		{
			$condition .= " AND shops.city = " . $dropdownCity;
		}

		if($dropdownState !="")
		{
			$condition .= " AND shops.state = " . $dropdownState;
		}

		$sql .= $condition;
		$sql .= "  group by VO.unit ";
		$result1 = mysqli_query($con,$sql);
		$Cat_totalquantity = 0; $Cat_totalcost = 0;
		
		while($row = mysqli_fetch_array($result1))
		{
			$Cat_totalcost = floatval( $Cat_totalcost) + floatval($row["totalcost"]);
			$AllCat_totalcost = floatval($AllCat_totalcost) + floatval($row["totalcost"]);
			
			if(SALESREPORT_TYPE=="KG") {
				$totalquantity = floatval($row["totalquantity"]);
				if($row["unit"]=="g" || $row["unit"]=="gm") {
					$totalquantity = $totalquantity / 1000;
				}
				$Cat_totalquantity = floatval($Cat_totalquantity) + floatval($totalquantity);
				$AllCat_totalquantity = floatval($AllCat_totalquantity) + floatval($totalquantity);			
			}
			else 
			{
				$totalquantity = $row["TotalVariantunit"];
				$Cat_totalquantity = $Cat_totalquantity + $totalquantity;
				$AllCat_totalquantity = $AllCat_totalquantity + $totalquantity;
			}
		}
		
		$returnString .= "<td style='text-align:right;'>". $Cat_totalquantity . "</td>";
		$arrReturn["Cat_totalquantity"][$i] = floatval($Cat_totalquantity);
		$arrReturn["Cat_totalcost"][$i] = floatval($Cat_totalcost);
	}
	
	$returnString .= "<th style='text-align:right;'>". floatval($AllCat_totalquantity) . "</th>"; 
	$arrReturn["returnString"] = $returnString;	
	return $arrReturn;
}
?>
<? if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}	
	.fa {
		height: 18px;
		display: inline-block;
		font: normal normal normal 14px/1 FontAwesome;
		font-size: inherit;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		transform: translate(0, 0);
	}
</style>
<? } ?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
	<? if($_GET["actionType"]!="excel") { ?>
		<div class="caption"><i class="icon-puzzle"></i>Monthly Sales Report</div>
		<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
		&nbsp;
		<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		<? } ?>
	</div>
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<?
			if($dropdownshops !="")
			{
				$sql=" SELECT  name from tbl_shops where id =  $dropdownshops";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$MarketName = $row["name"];
			} else {
				$MarketName = "ALL";
			}
			
			if($dropdownStockist !="")
			{
				$sql="SELECT firstname FROM tbl_user where id='".$dropdownStockist."' order by firstname";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$DistributorName = fnStringToHTML($row["firstname"]);
			} else {
				$DistributorName = "ALL";
			}
			
			if($dropdownSalesPerson !="")
			{
				$sql="SELECT firstname FROM tbl_user where id='".$dropdownSalesPerson."' order by firstname";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$SalesPersonName = fnStringToHTML($row["firstname"]);
			} else {
				$SalesPersonName = "ALL";
			}
			
			if($dropdownSuburbs !="")
			{
				$sql="SELECT suburbnm FROM tbl_surb WHERE id=$dropdownSuburbs ";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$SuburbName = fnStringToHTML($row["suburbnm"]);
			} else {
				$SuburbName = "ALL";
			} 
			
			$sql=" SELECT 
				tbl_brand.name, 
				tbl_brand.type, 
				tbl_brand.id, 
				tbl_category.categorynm,
				tbl_product.catid,
				count(tbl_category.brandid) as total_categories, 
				count(tbl_product.catid) as total_products 
			FROM 
				tbl_category 
				LEFT JOIN tbl_brand on tbl_brand.id = tbl_category.brandid 
				LEFT JOIN tbl_product on tbl_product.catid = tbl_category.id  ";
				
			$condition = " Where 1=1 ";
			if($dropdownbrands  !="")
			{
				$condition .= " AND tbl_category.brandid = " . $dropdownbrands;
			} 
			if($dropdownCategory  !="")
			{
				$condition .= " AND tbl_product.catid = " . $dropdownCategory;
			}
			if($dropdownProducts  !="")
			{
				$condition .= " AND tbl_product.id = " . $dropdownProducts;
			}
			
			$sql .= $condition;
			
			$sql .= " GROUP By categorynm having total_products > 0  ORDER BY  tbl_brand.type,  tbl_brand.name,  tbl_category.categorynm";
			
			$result1 = mysqli_query($con,$sql);
			$type = ""; 
			$i=0;
			$counter = 0;
			
			$totalColumn=mysqli_num_rows($result1);
			
			$totalColumn += 4;
			$firstColumn = round($totalColumn/2);
			$SecondColumn = $totalColumn - $firstColumn;
			$SecondAColumn = round($SecondColumn/2);
			$SecondBColumn = $SecondColumn - $SecondAColumn;
			
			?>
			<table class="table table-striped table-hover table-bordered" width="100%">
				<thead>
				<tr>
					<th colspan="<?=$firstColumn;?>">Sales Person: <?=$SalesPersonName;?></th>
					<th colspan="<?=$SecondColumn;?>">From Date: <?=$StartDate;?> To Date: <?=$endDate;?> </th>
				</tr>
				<tr>
					<th colspan="<?=$firstColumn;?>">Distributor Name: <?=$DistributorName;?></th>
					<th colspan="<?=$SecondAColumn;?>">Area: <?=$SuburbName;?></th>
					<th colspan="<?=$SecondBColumn;?>">Month: <?=$MonthName;?></th>
				</tr>
				 
					<tr>
						<th>SR.</th>
						<th>Date</th>
						<th>SHOP</th>
						<?php	
						
						while($row = mysqli_fetch_array($result1))
						{
							if($type != $row["type"] && $i!=0) {
							?><th style="text-align:center;" colspan="<?=$i;?>"><?=$type;?></th><?	
							$i=0;
							}
							$type = $row["type"];
							$arrProductInfo[$counter]['brand'] = fnStringToHTML($row["name"]);
							$arrProductInfo[$counter]['category'] = fnStringToHTML($row["categorynm"]);
							$arrProductInfo[$counter]['catid'] = $row["catid"];
							$i++;
							$counter++;
						}
						?><?php
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:center;"><?=$arrProductInfo[$i]['brand'];?></th><? } ?>
						<th>&nbsp;</th>
					</tr>
					
					<!--<tr>
						<th>No.</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<?php
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:center;"><?=$arrProductInfo[$i]['brand'];?></th><? } ?>
						<th>&nbsp;</th>
					</tr>-->
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<?php
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:center;"><?=$arrProductInfo[$i]['category'];?></th><? } ?>
						<th>Total(Pcs)</th>
					</tr>

								
					
				</thead>
				<tbody>
					<? 
					for($i=0;$i<$maxDays;$i++) { ?>
					<tr>
						<? $dateToShow =  date('d-m-Y', strtotime($StartDate . ' +'.$i.' day')); ?>
						<td><?=$i+1;?></td>
						<td><?=$dateToShow;?></td>
						<td><?=$MarketName;?></td>
						<? $arrReturn =  getProductData($con, $counter , $dateToShow , $arrProductInfo, $_POST);
							echo $arrReturn["returnString"];
							$AllarrReturn[$i] = $arrReturn;
						?>
					</tr>
					<? } ?> 
					<tr>
						<td>&nbsp;</td>
						<th>Total(Pcs)</th>
						<td>&nbsp;</td>
						<?php
							$OverAllTotal = 0;
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:right;">
								<?
								$catSum = 0;
								for($j=0;$j<$maxDays;$j++) {
									$catSum = $catSum + $AllarrReturn[$j]["Cat_totalquantity"][$i];
								}
								$OverAllTotal = $OverAllTotal + $catSum;
								echo $catSum;
								?>
								</th><? } ?>
						<th style="text-align:right;"><?=$OverAllTotal;?></th>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
						<th>VALUE <? if($_GET["actionType"]=="excel") { ?>RS<?}else{?>₹<?}?></th>
						<td>&nbsp;</td>
						<?php
							$OverAllTotal = 0;
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:right;">
								<?
								$catSum = 0;
								for($j=0;$j<$maxDays;$j++) {
									$catSum = $catSum + $AllarrReturn[$j]["Cat_totalcost"][$i];
								}
								$OverAllTotal = $OverAllTotal + $catSum;
								echo $catSum;
								?>
								</th><? } ?>
						<th style="text-align:right;"><?=$OverAllTotal;?></th>
					</tr>
					
				 </tbody>
			</table>
		</div>
	</div>
</div> 
<?
if($_GET["actionType"]=="excel") {	
	$FileName = date('F-Y', strtotime($StartDate . ' +0 day')); 
	header( "Content-Type: application/vnd.ms-excel");
	header( "Content-disposition: attachment; filename=Report_".$FileName.".xls");
} ?>