<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") {
	header("location:../logout.php");
} 
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Stockist";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Stockist</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Stockist</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Stockist Listing
							</div>
                            <a href="distributor-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Stockist
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<!--<th>
									 Superstockist
								</th>-->
								<th>
									 Name
								</th>								
                                <th>
									 Email
								</th>
                                <th>
									 Mobile Number
								</th>
								<th>
                                	Area
                                </th>
                                <th>
                                	City
                                </th>
                                <th>
                                  State
                                </th>
                                <?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") 
								{ ?>
								<th>
                                  Action
                                </th>
								<?php } ?>
							</tr>
							</thead>
							<tbody>
						<?php
						$user_type="Distributor";
						switch($_SESSION[SESSION_PREFIX.'user_type']){
							case "Admin":								
								$external_id = "";
							break;
							case "Superstockist":								
								$external_id = $_SESSION[SESSION_PREFIX.'user_id'];
							break;
						}
						$result1 = $userObj->getAllLocalUserDetails($user_type,$external_id);
						//$result1 = mysqli_query($con,$sql);
						while($row = mysqli_fetch_array($result1))
						{
							$sqlparent="SELECT firstname FROM `tbl_user` where id ='".$row['external_id']."'";
							$result1parent = mysqli_query($con,$sqlparent);
							$rowparent = mysqli_fetch_array($result1parent);
						
							$working_area_details = $userObj->getLocalUserWorkingAreaDetails($row['id']);
							if($working_area_details == 0){								
								$row['state_ids'] = $row['state'];									
								$row['city_ids'] = $row['city'];
							}elseif(count($working_area_details) > 0){	
								$row = array_merge($row,$working_area_details);
							}
							echo '<tr class="odd gradeX">';
								
								echo '<td><a href="distributor1.php?id='.$row['id'].'">'.fnStringToHTML($row['firstname']).'</a></td>';
                                
                              
								 echo '<td>'.fnStringToHTML($row['email']).'</td>
                                <td>'.fnStringToHTML($row['mobile']).'</td>
								<td>';								
								if(!empty($working_area_details['suburb_ids'])){
									$sql="SELECT GROUP_CONCAT(suburbnm) AS all_suburb FROM tbl_surb where id IN(".$working_area_details['suburb_ids'].")";
									$result = mysqli_query($con,$sql);
									$row_suburb = mysqli_fetch_assoc($result);
									echo  $row_suburb['all_suburb'];
								}else{
									echo '-';
								}
								echo '</td>
                                <td>';
								$city_id=$row['city_ids'];
								if(!empty($city_id)){
									$sql="SELECT name FROM tbl_city where id = $city_id";
									$result = mysqli_query($con,$sql);
									while($num = mysqli_fetch_array($result))
									{ 
										echo  $num['name'];
									}
								}else{
									echo '-';
								}
								echo '</td><td>';
								$state_id=$row['state_ids'];
								if(!empty($state_id)){
									$sql="SELECT name FROM tbl_state where id = $state_id";
									$result = mysqli_query($con,$sql);
									while($num = mysqli_fetch_array($result))
									{ 
										echo  $num['name'];
									}
								}else{
									echo '-';
								}
								echo '</td>';
								if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {
									echo '<td>
										<a href="manageuser.php?utype=Stockist&id='.$row['id'].'">Delete</a>
									</td>';
								}
									
								echo '</tr>';
							} ?>
						
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>