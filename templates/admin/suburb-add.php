<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Area";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Area</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="suburb.php">Area</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Area</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Area
							</div>							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						<?php
						if(isset($_POST['submit'])){
							$suburbnm=fnEncodeString($_POST['suburbnm']);
							$state=$_POST['state'];
							$city =$_POST['city'];
							
							$sql_product_check=mysqli_query($con,"select id from `tbl_surb` where suburbnm='$suburbnm' and cityid='$city'");
	
							if($rowcount = mysqli_num_rows($sql_product_check)>0){	
								echo '<script>alert("Duplicate Area not added.");location.href="suburb-add.php";</script>';
							}else{
								$sql_insert = "INSERT INTO tbl_surb (`suburbnm`,`cityid`,`stateid`) VALUES('".$suburbnm."','".$city."','".$state."')";
								$sql1 = mysqli_query($con,$sql_insert);
								$record_id = mysqli_insert_id($con); 
								$commonObj 	= 	new commonManage($con,$conmain);
								$commonObj->log_add_record('tbl_surb',$record_id,$sql_insert);	
								echo '<script>alert("Area added successfully.");location.href="suburb.php";</script>';
							}
							
							
						} ?>  
						<form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="suburb-add.php">         
						<div class="form-group">
						  <label class="col-md-3">State:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
						  <select name="state"              
						  data-parsley-trigger="change"				
						  data-parsley-required="#true" 
						  data-parsley-required-message="Please select state"
						  class="form-control" onChange="showUser(this.value)">
						  <option selected disabled>-select-</option>
							<?php
							$sql="SELECT * FROM tbl_state where country_id=101";
							$result = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result))
							{
								$cat_id=$row['id'];
								echo "<option value='$cat_id'>" . $row['name'] . "</option>";
							} ?>
							</select>
						  </div>
						</div><!-- /.form-group -->
						
						<div id="Subcategory"></div> 
						
						<div class="form-group">
						  <label class="col-md-3">Area Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text" 
							placeholder="Enter Area Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Area name"
							data-parsley-maxlength="50"
							data-parsley-maxlength-message="Only 50 characters are allowed"
							name="suburbnm"class="form-control">
						  </div>
						</div><!-- /.form-group -->
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
							<a href="suburb.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
					  </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function showUser(str)
{
if (str=="")
{
document.getElementById("Subcategory").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetch.php?cat_id="+str,true);
xmlhttp.send();
}
</script>            