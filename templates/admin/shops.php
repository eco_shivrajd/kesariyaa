<!-- BEGIN HEADER -->
<?php header("Cache-Control: no-store, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
include "../includes/grid_header.php";
include "../includes/shopManage.php";
include "../includes/userManage.php";
$shopObj 	= 	new shopManager($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Shops";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Shops</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Shops</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Shop Listing</div>
							
							<? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
								<a href="shops-add.php" class="btn btn-sm btn-default pull-right mt5">Add Shop</a>
							<? } ?>
							
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th>
											 Shop Name
										</th>
										
										<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ ?>
										<th>
											 Stockist
										</th>
										<?php } ?>
										<th>
											 Contact Person
										</th>
										<th>
											 Mobile Number
										</th>
										<th>
											Area
										</th>
										<th>
											City
										</th>
										<th>
											State
										</th>
										<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") 
										{ ?>
										<th>
										  Action
										</th>
										<?php } ?>
									</tr>
								</thead>
							<tbody>
							<?php
							$result1 = $shopObj->getAllShopsByUser();
							while($row = mysqli_fetch_array($result1))
							{
								$sstockist = $userObj->getLocalUserDetails($row['sstockist_id']);
								$stockist = $userObj->getLocalUserDetails($row['stockist_id']);
								$display_icon = '';
								$display_icon_status = '';
								if($row['status'] != 0){//Shop added by Sales Person
									$shop_data = $shopObj->getShopStatusNLog($row['id']);
									$current_log_array = json_decode($shop_data['status_seen_log']);
									if($current_log_array != '')
										$current_log_array = get_object_vars($current_log_array); 
									switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":	
											if($shop_data['status_seen_log'] == '')
												$display_icon_status = 1;
											else if(is_array($current_log_array['status']) && in_array(4,($current_log_array['status'])) !=1)//shop details not seen by Admin
												$display_icon_status = 1;											
										break;
										case "Superstockist":
											if($shop_data['status_seen_log'] == '')
												$display_icon_status = 1;
											else if(is_array($current_log_array['status']) && in_array(3,($current_log_array['status'])) != 1)//shop details not seen by Superstockist
												$display_icon_status = 1;
											
										break;
										case "Distributor":
											if($shop_data['status_seen_log'] == '')
												$display_icon_status = 1;
											else if(is_array($current_log_array['status']) && in_array(2,($current_log_array['status'])) != 1)//shop details not seen by Distributor
												$display_icon_status = 1;
											
										break;
									}
								}
								if($display_icon_status == 1)
									$display_icon = '<span style="float: right"><img src="'.SITEURL.'/assets/global/img/new-icon.png" title="New Shop"></span>';											

								echo '<tr class="odd gradeX">
								<td>
									<a href="shops1.php?id='.$row['id'].'">'.fnStringToHTML($row['name']).'</a>'.$display_icon.'
								</td>';     
								
								if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ 				
									echo '<td>
										  '.$stockist['firstname'].'
									</td>'; 
								}								
                                echo '<td>'.fnStringToHTML($row['contact_person']).'</td>
                                <td>'.$row['mobile'].'</td><td>';
                                $area_id=$row['suburbid'];
									if($area_id){
										$sql="SELECT * FROM tbl_surb where id = $area_id";
										$result = mysqli_query($con,$sql);
										while($num = mysqli_fetch_array($result))
										{ 
											echo  $num['suburbnm'];
										}
									}else{
										echo "";
									}
                                echo '</td><td>';
								$city_id=$row['city'];						
								$sql="SELECT * FROM tbl_city where id = $city_id";
								$result = mysqli_query($con,$sql);
								while($num = mysqli_fetch_array($result))
								{ 
									echo  $num['name'];
								}									
								echo '</td><td>';
								
								$state_id=$row['state'];						
								$sql="SELECT * FROM tbl_state where id = $state_id";
								$result = mysqli_query($con,$sql);
								while($num = mysqli_fetch_array($result))
								{ 
									echo  $num['name'];
								}
								echo '</td>';
								if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {
									echo '<td>
										<a href="manageuser.php?utype=Shops&id='.$row['id'].'">Delete</a>
									</td>';
								}
								echo '</tr>';
							} ?>
	
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>