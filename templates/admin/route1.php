<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
?>
<!-- END HEADER -->

<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageDeliveries"; $activeMenu = "Routes";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<h3 class="page-title">Sales Person</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="routes.php">Route</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Route</a>
					</li>
				</ul>

			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Route
							</div>							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						  
						<form class="form-horizontal" data-parsley-validate="" role="form" method="post" >         
						<div class="form-group">
						  <label class="col-md-3">State:</label>
						  <div class="col-md-4">
								<select name="state" class="form-control" onchange="fnShowCity(this.value)" data-parsley-trigger="change" data-parsley-required="#true" data-parsley-required-message="Please select State" readonly="" disabled="">
									<option value="">-Select-</option>
									<?php 									
									if(isset($_GET['id']))
									{
										$id=$_GET['id'];
										$getroute="SELECT id,name,shop_ids as ids FROM `tbl_route` where id=$id";
										$resultroute = mysqli_query($con,$getroute);
										$arr=array();
										while($row = mysqli_fetch_array($resultroute))
										{
											$arr[$row['id']]=explode(",", $row['ids']);
											$temp=$arr[$row['id']][0];
											
											 $getcitystate="SELECT 	tbl_shops.id as shopid,																	
																	tbl_shops.state as stateid,
																	tbl_state.name as state,
																	tbl_shops.city as cityid,
																	tbl_city.name as city ,
																	tbl_shops.suburbid as areaid,
																	tbl_surb.suburbnm as area,
																	tbl_shops.subarea_id as subarea_id,
																	tbl_subarea.subarea_name as subarea
																	
											FROM tbl_shops 
											left JOIN tbl_state ON tbl_shops.state = tbl_state.id
											 left JOIN tbl_city ON tbl_shops.city = tbl_city.id
											 left JOIN tbl_surb ON tbl_shops.suburbid = tbl_surb.id
											 left JOIN tbl_subarea ON tbl_shops.subarea_id = tbl_subarea.subarea_id
											 where tbl_shops.id = $temp ";
											$resultstatecity = mysqli_query($con,$getcitystate);											
											while($rowstatecity = mysqli_fetch_array($resultstatecity))
											{
												//echo $rowstatecity;
											
										if($rowstatecity['state']!=''){?>
											<option value="<?php echo $rowstatecity['stateid'];?>" selected><?php echo $rowstatecity['state'];?></option>
										<?php
											}else{
												$getcitystate="SELECT * from tbl_state	";
											$resultstatecity = mysqli_query($con,$getcitystate);											
											while($rowstatecity = mysqli_fetch_array($resultstatecity))
											{
										?>
										<option value="<?php echo $rowstatecity['stateid'];?>" selected><?php echo $rowstatecity['state'];?></option>
											<?php }
										}
									
									?>
									
								</select>
						  </div>
						</div><!-- /.form-group -->

						<div class="form-group" id="city_div">
						  <label class="col-md-3">City:</label>
						  <div class="col-md-4" id="div_select_city">
							<select name="city" class="form-control" data-parsley-trigger="change" data-parsley-required="#true" data-parsley-required-message="Please select City" readonly="" disabled="">
								<option value="">-Select-</option>
								<?php if($rowstatecity['city']!=''){?>
								<option value="<?php echo $rowstatecity['cityid'];?>" selected ><?php echo $rowstatecity['city'];?></option>
								<?php } ?>
							</select>
						  </div>
						</div><!-- /.form-group --> 

						<div class="form-group" id="area_div">
						  <label class="col-md-3">Area:</label>
												  <div class="col-md-4" id="div_select_area">
						  <select name="area" id="area" class="form-control" onchange="FnGetSubareaDropDown(this)" readonly="" disabled="">	
							<option value="">-Select-</option>
							<?php if($rowstatecity['area']!=''){?>
								<option value="<?php echo $rowstatecity['areaid'];?>" selected ><?php echo $rowstatecity['area'];?></option>
								<?php } ?>

							</select>
						  </div>
						</div><!-- /.form-group -->
						<div class="form-group" id="subarea_div">
						  <label class="col-md-3">Subarea:</label>
						  						  <div class="col-md-4" id="div_select_subarea">
						  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control" readonly="" disabled="">	
								<option value="">-Select-</option>
								<?php if($rowstatecity['subarea']!=''){?>
								<option value="<?php echo $rowstatecity['subarea_id'];?>" selected ><?php echo $rowstatecity['subarea'];?></option>
								<?php } ?>

							</select>
						  </div>
						</div><!-- /.form-group --> 
						
						<div class="form-group">
						  <label class="col-md-3">Route Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<?php if($row['name']!=''){?>
								<input type="text" id="routename" class="form-control" value="<?php echo fnStringToHTML($row['name']); ?>">
								<?php } ?>
							
						  </div>
						</div><!-- /.form-group -->
							<div class="form-group">
							<label class="col-md-3">Assign Shops:</label>
							<div class="col-md-9">
								<div class="row">
									<div class="col-sm-12">
										<p> Select shop record to drag and drop to assigned list</p>
									</div>
									<div class="col-sm-4">
										<p><b>Select Shop(s)</b></p>
										<ul id="shopslist" class="list-group list-group-sortable-connected maxhts">
							<?php 
							$temp2=array();
							$temp2=$arr[$row['id']];
							$where_clause = "";
							if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin')
							{
								if($rowstatecity['stateid'] != "" ){
								$where_clause = " WHERE ";}
							}
							if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist')
							{
								$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
								$where_clause = " WHERE (sstockist_id = '$user_id' OR stockist_id IN (SELECT id FROM tbl_user WHERE external_id = ".$user_id." )) and";
							}
							else if($_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
							{
								$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
								$where_clause = " WHERE stockist_id = '$user_id' and ";
							}
							//echo $_GET['state'];
							if($rowstatecity['stateid'] != "" ){
								$where_clause .="  state=".$rowstatecity['stateid'].' ';
								if($rowstatecity['city'] != ""){
									$where_clause .=" and city=".$rowstatecity['cityid'].' ';
									if($rowstatecity['area'] != ""){
										$where_clause .="and suburbid = ".$rowstatecity['areaid'].' ';
										if($rowstatecity['subarea'] != ""){
											$where_clause .=" and subarea_id = ".$rowstatecity['subarea_id'].' ';
										}
									}
								}
							}

						  $sqlsup="SELECT tbl_shops.name as name,tbl_shops.id as id
							FROM tbl_shops left JOIN tbl_state ON tbl_shops.state = tbl_state.id
							 left JOIN tbl_city ON tbl_shops.city = tbl_city.id
							 ".$where_clause ." and tbl_shops.id not in ( '" . implode($temp2, "', '") . "' )";

						$resultassign = mysqli_query($con,$sqlsup);
						while($rowassign = mysqli_fetch_array($resultassign))
						{ ?>
							<li class="list-group-item" draggable="true" id="<?php echo $rowassign['id'];?>"><?php echo fnStringToHTML($rowassign['name']);?></li>
						<?php } ?>	
										</ul>
									</div>
									
									<div class="col-sm-4">
										<p><b>Assigned Shop(S)</b></p>
										<ul id="shopsassign" class="list-group list-group-sortable-connected maxhts">
										<?php 
										
										$sqlforassgnd="SELECT tbl_shops.name as name,tbl_shops.id id 
										FROM tbl_shops 
										 WHERE id in ( '" . implode($temp2, "', '") . "' )";										
										
										$resultforassgnd = mysqli_query($con,$sqlforassgnd);
										while($rowforassgned = mysqli_fetch_array($resultforassgnd)){
										?>
											<li class="list-group-item" draggable="true" id="<?php echo $rowforassgned['id'];?>"><?php echo fnStringToHTML($rowforassgned['name']);?></li>
											<?php } ?>
											
										</ul>
									</div>
									
								</div>
							</div>
						</div>
						<?php 
						}
										}
									}
						?>
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="button" name="submit" id="submit" class="btn btn-primary"  onclick="javascript: return editroute()">Submit</button>
							<a href="routes.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
					  </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<script src="../../assets/admin/pages/scripts/jquery.sortable.js"></script>
<!-- END FOOTER -->
<!-- END PAGE LEVEL SCRIPTS -->
<script>
		$(function() {
            $('.list-group-sortable-connected').sortable({
                placeholderClass: 'list-group-item',
                connectWith: '.connected'
            });
		});
	
	function editroute(){
		var routeid;
		routeid = <? print $_GET['id']; ?>;		
		var ids="";
		$('#shopsassign li').each(function(i)
			{
			   ids+=$(this).attr('id').toString()+","; 
			});
		var routename=$('#routename').val();
		//
		if(ids!=''){
			var url = 'route-update.php?routeid='+routeid+'&shopids='+ids+'&routename='+routename;
			 $.ajax({
				type: 'post',
				url: url,
				datatype:"text",
				success: function (data) {
					if(data=='duplicate'){
						alert("Duplicate route name not allowed.");
						location.reload();
					}else{
						 alert("Route updated successfully.");
						  window.location='routes.php';
						  return false;
					}
				}
			  });
		}else{
				alert("Minimum one shop required in Assigned Shop(S) to update.");
				location.reload();
		}
		return false;
		}
		

function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');		
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetSubareaDropDown";
	CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) {	
	var suburb_str = $("#area").val();
	$("#subarea_div").show();	
	var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea";
	CallAJAX(url,"div_select_subarea");
}
	</script>
<!-- END JAVASCRIPTS -->

<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
