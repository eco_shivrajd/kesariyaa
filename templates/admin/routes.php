<!-- BEGIN HEADER -->
<?php 
include "../includes/grid_header.php";
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageDeliveries"; $activeMenu = "Routes";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Route</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Route</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Route Listing
							</div>
                            <a href="route-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Route
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_2" role="grid" aria-describedby="sample_2_info">
							<thead>
							<tr role="row"><th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
									 Route Name
								: activate to sort column ascending" style="width: 526px;">
									 Route Name
								</th><th class="sorting_asc" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
									City
								: activate to sort column ascending" aria-sort="ascending" style="width: 167px;">
									City
								</th><th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
									State
								: activate to sort column ascending" style="width: 294px;">
									State
								</th>
								</tr>
							</thead>
							<tbody>
						<?php
						if($_SESSION[SESSION_PREFIX.'user_type']=="Admin"){								
							$getroute="SELECT id,name,shop_ids as ids FROM `tbl_route` ";
						}else{
							$uid=$_SESSION[SESSION_PREFIX.'user_id'];
							$getroute="SELECT id,name,shop_ids as ids FROM `tbl_route` where distributor_id='$uid'";
						}
					
						$resultroute = mysqli_query($con,$getroute);
						$arr=array();
						while($row = mysqli_fetch_array($resultroute))
						{
							$arr[$row['id']]=explode(",", $row['ids']);
							$temp=$arr[$row['id']][0];
							 $getcitystate="SELECT tbl_shops.id,tbl_state.name as state,tbl_city.name as city 
							FROM tbl_shops 
							left JOIN tbl_state ON tbl_shops.state = tbl_state.id
							 left JOIN tbl_city ON tbl_shops.city = tbl_city.id
							 where tbl_shops.id=$temp";
							$resultstatecity = mysqli_query($con,$getcitystate);
							while($rowstatecity = mysqli_fetch_array($resultstatecity))
							{
						?>
							<tr role="row" class="odd">
								<td>
									 <a href="route1.php?id=<?php echo $row['id'];?>"><?php echo $row['name'];?></a>
								</td>
								<td class="sorting_1">
									<?php echo $rowstatecity['city'];?>
								</td>
								<td>
									<?php echo $rowstatecity['state'];?>
								</td>
							</tr>
							<?php 
							//echo "<pre>";print_r($rowstatecity);
							}
						}
							?>
							</tbody>
							</table></div></div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->

</body>

<!-- END BODY -->
</html>