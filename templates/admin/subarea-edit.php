<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}
$id	= $_GET['id'];
if(isset($_POST['submit'])) {	
	$txtsubarea = fnEncodeString($_POST['txtsubarea']);	
	$state	 	= $_POST['state'];
	$city 	 	= $_POST['city'];
	$suburbnm 	= $_POST['suburbnm'];
	
	$sql_subarea_check=mysqli_query($con,"select subarea_id from `tbl_subarea` where subarea_name='$txtsubarea' and city_id='$city'");

	if($rowcount = mysqli_num_rows($sql_subarea_check)>0){	
		echo '<script>alert("Duplicate Subarea not added.");location.href="subarea-edit.php?id='.$id.'";</script>';
	}else{
		$sql = "UPDATE tbl_subarea SET 
		state_id = '$state',
		city_id  = '$city',
		suburb_id= '$suburbnm',
		subarea_name='$txtsubarea'
		WHERE subarea_id = '$id'";
		
		$update_sql=mysqli_query($con,$sql);
		echo '<script>alert("Subarea updated successfully."); location.href="subarea.php";</script>';
		//exit;
	}
	
}

$sql1	 =	"SELECT suburb_id, city_id, state_id, subarea_name FROM tbl_subarea where subarea_id = '$id' ";
$result1 = mysqli_query($con,$sql1);
$row1 	 = mysqli_fetch_array($result1);
$state_id= $row1["state_id"];
$city_id = $row1['city_id'];
$suburb_id = $row1['suburb_id'];
$subareanm = fnStringToHTML($row1['subarea_name']);

?>

<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Subarea";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- /.modal -->			
			<h3 class="page-title">Subarea</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="subarea.php">Subarea</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li><a href="#">Edit Subarea</a></li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Subarea
							</div>	
							
														
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						  
						<form class="form-horizontal" data-parsley-validate="" role="form" method="post">         
						<div class="form-group">
						  <label class="col-md-3">State:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
						  <select name="state" data-parsley-trigger="change" data-parsley-required="#true" data-parsley-required-message="Please select state"
						  class="form-control" onChange="getCityDropCities(this.value)">
						  <option selected disabled>-select-</option>
							<?php
							$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
							$result = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result))
							{
								$cat_id=$row['id'];
								if($state_id == $cat_id)
									$sel="SELECTED";
								else
									$sel="";
								echo "<option value='$cat_id' $sel>" . $row['name'] . "</option>";
							} ?>
							</select>
						  </div>
						</div><!-- /.form-group -->
						
						<div id="Subcategory"></div>
						<div id="div_subarea"></div>
						
						<div class="form-group">
						  <label class="col-md-3">Subarea Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text" 
							placeholder="Enter Subarea Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter subarea name"
							data-parsley-maxlength="50"
							data-parsley-maxlength-message="Only 50 characters are allowed"
							name="txtsubarea"class="form-control" value="<?=$subareanm;?>">
						  </div>
						</div><!-- /.form-group -->
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
						   
						   <button type="button" name="btnDelete" id="btnDelete" class="btn btn-danger pull-right" onclick="javascript: fnDelete()">Delete</button>
						   
							<a href="subarea.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
					  </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<style>
.form-horizontal{
	font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function getCityDropCities(str,action)
{
	if (str=="")
	{
		document.getElementById("Subcategory").innerHTML="";
		return;
	}
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
			if(action=="onload") {
				$("#city").val("<?=$city_id;?>");
				showSuburb('<?=$city_id;?>','onload');
			}
		}
	}
	xmlhttp.open("GET","fetch.php?cat_id="+str,true);
	xmlhttp.send();
}

getCityDropCities('<?=$state_id;?>','onload');

function showSuburb(str,action)
{
	if (str=="")
	{
		document.getElementById("div_subarea").innerHTML="";
		return;
	}
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("div_subarea").innerHTML=xmlhttp.responseText;
			if(action == "onload") {
				$("#suburbnm").val("<?=$suburb_id;?>");
			}
		}
	}
	xmlhttp.open("GET","fetch_suburb.php?cat_id="+str,true);
	xmlhttp.send();
}
function fnDelete()
{	
	if(confirm("Are you sure you want to delete this subarea?")) {
		if (window.XMLHttpRequest) {
			xmlhttp=new XMLHttpRequest();
		}
		else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				if(xmlhttp.responseText=="DONE") {
				alert("subarea has been deleted successfully.");
				location.href="subarea.php";
				} else {
					alert("This subarea is already assigned to respective data, Please unassign first associative data to delete this record.");
					//location.href="subarea.php";
				}
			}
		}
		xmlhttp.open("GET","deletesubarea.php?subarea_id=<?=$id;?>",true);
		xmlhttp.send();
	}
}
</script>