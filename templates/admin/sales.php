<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
//$userObj->migration_sp_sstockist(); exit;
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "SalesPerson";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Sales Person
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Sales Person</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Sales Persons Listing
							</div>
                            <a href="sales-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Sales Person
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th width="15%">
									 Name
								</th>	
								<th width="25%">
									Assigned Stockist
								</th>								
                                <th width="20%">
									 Email
								</th>
                                <th width="10%">
									 Mobile Number
								</th>
								<th width="10%">
                                	Area
                                </th>    
								<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ ?>
								<th width="10%">
                                	Action
                                </th>
								<?php } ?>
							</tr>
							</thead>
							<tbody>
							<?php
							
							switch($_SESSION[SESSION_PREFIX.'user_type']){
								case "Admin":									
									$user_type="SalesPerson";									
									$external_id = "";
									break;
								case "Superstockist":
									$user_type="SalesPerson";		
									$external_id = $_SESSION[SESSION_PREFIX.'user_id'];									
								break;
								case "Distributor":								
									$user_type="SalesPerson";									
									$external_id = $_SESSION[SESSION_PREFIX.'user_id'];
								break;
							}
							
							$result1 = $userObj->getAllLocalUserDetails($user_type,$external_id);						
							while($row = mysqli_fetch_array($result1))
							{
								$row_count = 0;
								$external_id = $row['external_id'];
								$assign_stockist = "";
								if($external_id != ''){
									$sql_suburb="SELECT GROUP_CONCAT(suburb_ids) AS suburb_ids FROM tbl_user_working_area where user_id IN (".$external_id.") ";
									$result_suburb= mysqli_query($con,$sql_suburb);
									$row_count = mysqli_num_rows($result_suburb);
									$row_suburs = array();
									if($row_count > 0){	
										$row_suburs = mysqli_fetch_assoc($result_suburb);	//print"<pre>";print_r($row_suburs);	
									}	
									$sql_assign_stockist = "SELECT GROUP_CONCAT(firstname) AS assign_stockist FROM tbl_user WHERE id IN (". $external_id.") and isdeleted!='1'";
									$result_assign_stockist = mysqli_query($con,$sql_assign_stockist);
									$row_count_assign_stockist = mysqli_num_rows($result_assign_stockist);
									$rec_assign_stockist = mysqli_fetch_assoc($result_assign_stockist);
									//print"<pre>"; print_r($rec_assign_stockist);
									$assign_stockist = str_replace(',',', ',$rec_assign_stockist['assign_stockist']);
								}
								echo '<tr class="odd gradeX">
								<td>
									 <a href="sales1.php?id='.$row['id'].'">'.fnStringToHTML($row['firstname']).'</a>
								</td>'; 
                              
								echo '<td>'.$assign_stockist.'</td>
								<td>'.$row['email'].'</td>
                                <td>'.$row['mobile'].'</td>
                                <td>';								
								if(!empty($row_suburs['suburb_ids']) && $row_count > 0){									
									$suburbs = str_replace(',,',',',$row_suburs['suburb_ids']);
									$suburbs = rtrim($suburbs,",");
									$suburbs = ltrim($suburbs,",");
									$sql_s_name="SELECT GROUP_CONCAT(suburbnm) AS all_suburb FROM tbl_surb where id IN(".$suburbs.")";
									$result_s_name = mysqli_query($con,$sql_s_name);
									$suburb_count = mysqli_num_rows($result_s_name);
									$row_suburb = mysqli_fetch_assoc($result_s_name);
									echo  $row_suburb['all_suburb'];
								}else{
									echo '-';
								}
								echo '</td>';
								
							?>
							<div id="stockist_dropdown_<?=$row['id'];?>" style="display:none;"> 
								<select name="assign[]" multiple class="form-control">				 
								<?php
								$user_type="Distributor";
								$parentid = $row['sstockist_id'];								
								$result_stockist = $userObj->getLocalUserDetailsByUserType($user_type,$parentid);
								if($result_stockist != 0){
									while($row_stockist = mysqli_fetch_array($result_stockist))
									{
										$assign_id=$row_stockist['id'];
										$external_ids = explode(',',$row['external_id']);
										if(in_array($assign_id,$external_ids))
											$sel="SELECTED";
										else
											$sel="";
										echo "<option value='$assign_id' $sel>" . fnStringToHTML($row_stockist['firstname']) . "</option>";
									}
								}	?>
								</select>
							</div>
							<?php	
							if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ 
								$assign_link = "-";
								if($result_stockist != 0){
									$assign_link = '<a onclick="javascript: assign_stockist('.$row['id'].')">Assign</a>';
								$assign_link1=	'<a href="manageuser.php?utype=SalesPerson&id='.$row['id'].'">Delete</a>';
								}
								echo '<td>'.$assign_link.'/'.$assign_link1.'</td>';
							}
								echo '</tr>';
							} ?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<div class="modal fade" id="assignStockistModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Assign Stockist</h4>	   
      </div>		
    <form role="form" class="form-horizontal" onsubmit="return false;" action="sales.php" data-parsley-validate="" name="assign_stockist_form" id="assign_stockist_form">
      <div class="modal-body" >	  
	  <div class="clearfix"></div>
			<div class="form-group">
			<label class="col-md-3">Select Stockist:</label>
			<div class="col-md-4" id="show_stockist_dropdown">
				
			</div>
			</div><!-- /.form-group --> 			
			<div class="form-group">
				<div class="col-md-4 col-md-offset-3">					
					<button type="submit"  name="btnsubmit"  class="btn btn-primary">Submit</button>
					<a href="sales.php" class="btn btn-primary">Cancel</a>
				</div>
			</div><!-- /.form-group -->
			<input type="hidden" name="sales_p_id" id="sales_p_id" value="0"/>
			<input type="hidden" name="action" id="action" value="assign_stockist"/>
		</div><!-- /.form-group --> 				
      </div>
	   </form>	   	  
    </div>
  </div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function assign_stockist(sales_p_id){	
	$('#sales_p_id').val(sales_p_id);
	var select_drop = $('#stockist_dropdown_'+sales_p_id).html();
	$('#show_stockist_dropdown').html(select_drop);
	$('#assignStockistModal').modal('show');
}
$('form#assign_stockist_form').submit(function(){
	var formData = new FormData($(this)[0]);	
	$.ajax({
		url:"../includes/assign_stockist.php",
		type: 'POST',
		data: formData,
		success: function (data) {
			if(data == 1){
				alert('Stockist assigned successfully');
				$('#assignStockistModal').modal('hide');
				document.forms.assign_stockist_form.reset();
				window.location.reload(true);
			}else if(data == 'select'){
				alert('Please select Stockist');
				return false;
			}else{
				alert('Unable to assign Stockist');
				return false;
			}					
		},
		cache: false,
		contentType: false,
		processData: false
	});
});
</script>
</body>
<!-- END BODY -->
</html>