<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/commonManage.php";	
$commonObj 	= 	new commonManage($con,$conmain);
$commonObjctype 	= 	$commonObj->log_get_commonclienttype($con,$conmain);
?>
<!-- END HEADER -->
<?php
$recieved = $_GET["recieved"];
if(isset($_POST['action']) && $_POST['action'] == 'place_order')
{
	$user_id = $_SESSION[SESSION_PREFIX.'user_id'];	
	foreach ($_POST['orderid'] as $key => $orderid) {
		$sql_update = mysqli_query($con,"UPDATE `tbl_order_app` SET status = '3' WHERE superstockistid = '$user_id' and id = '$orderid' ");
	}
	header("location:Orders.php");
	//var_dump($_POST); exit;
} ?>

<style>
.form-horizontal .control-label {
    text-align: left;
}
</style>
<script>
function fnSelectionBoxTest()
{
	if(document.getElementById('selTest').value == '3')
	{
	   document.getElementById('date-show').style.display = "block";
	   document.getElementById('dvTestData').style.display = "none"; 
	}
	else if(document.getElementById('selTest').value == '2')
	{
	   document.getElementById('dvTest').style.display = "none";
	   document.getElementById('dvTestData').style.display = "block"; 
	}
	else
	{
	   document.getElementById('dvTest').style.display = "none";
	   document.getElementById('dvTestData').style.display = "none";
	}
}
</script>
</head>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Orders"; $activeMenu = "Orders";
	
	if($recieved=="admin") {
		$activeMenu = "OrderVisibility";
	}
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<h3 class="page-title">Orders</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Orders</a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
					<form class="form-horizontal">
						<div class="form-group">
							<div id="date-show" style="display:none;">
								<label for="inputEmail3" class="col-sm-2 control-label">From Date:</label>
								<div class="col-md-2">
									<div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
										<input type="text" class="form-control">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</div>

								<label for="inputEmail3" class="col-sm-2 control-label">To Date:</label>
								<div class="col-md-2">
									<div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
										<input type="text" class="form-control">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</div>
							</div>  
						</div>
					</form>	
                          
					 <div class="clearfix"></div>   
					<form class="form-horizontal" method="post" name="place_order_form">
						<input type="hidden" name="action" value="place_order">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
							<? if($recieved=="admin") {?>
								Order Visibility
							<?} else { ?>
								Order Received
							<?}?>
							</div>
                          
						</div>
						<div class="portlet-body">
			
						<table class="table table-striped table-bordered table-hover" id="sample_2">
						<thead>
							<tr>
							<th width="20%">Brand</th>
							<th width="25%">Category</th>
							<th width="25%">Product</th>
							<th width="10%">Free Quantity</th>
							<th width="10%">Sale Quantity</th>
							<th width="10%">Total Quantity</th>
							</tr>
						</thead>
						<tbody>	
							<?php
							switch($_SESSION[SESSION_PREFIX.'user_type']) {
								
								case "Admin":
								
									$statusCondition = "";
									$statusCondition_inside = "";
									if($recieved!="admin") {
										$statusCondition = " WHERE OV.status='3' ";
										$statusCondition_inside = " AND OV.status='3' ";
									}
								
									$sql = "SELECT *,SUM(OV.variantunit) as quantity FROM `tbl_order_app` AS OA LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id $statusCondition GROUP BY OV.product_varient_id";
									
									$result1 = mysqli_query($con,$sql);
									
									while($row = mysqli_fetch_array($result1)) {
										
										/*$sql_brand_category = "SELECT categorynm, (SELECT name FROM tbl_brand WHERE id = brandid) as brand FROM `tbl_category` WHERE tbl_category.id = ".$row['catid'];
										
										$brand_category = mysqli_query($con,$sql_brand_category);
										$obj_brand_cat = mysqli_fetch_object($brand_category);
										*/
										
										$sql_product = "SELECT  productname FROM `tbl_product` WHERE id = '".$row['productid'] ."'";
										
										$product_result = mysqli_query($con,$sql_product);
										$obj_product = mysqli_fetch_object($product_result);
										
										$sql_free_pro = "SELECT SUM(variantunit) as quantity_free FROM `tbl_variant_order` AS OV INNER JOIN `tbl_order_app` ON tbl_order_app.id = OV.orderappid WHERE campaign_sale_type='free' AND product_varient_id = '".$row['product_varient_id'] ."' $statusCondition_inside";
										
										$result_free_pro = mysqli_query($con,$sql_free_pro);
										$row_free_pro = mysqli_fetch_assoc($result_free_pro);
										$free_quantity = 0;
										$sale_quantity = $row['quantity'];
										if($row_free_pro['quantity_free'] > 0)
										{
											$free_quantity = $row_free_pro['quantity_free'];
											$sale_quantity = $row['quantity'] - $free_quantity;
										}
										
										$product_varient_id = $row['product_varient_id'];
										$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
										$resultprd = mysqli_query($con,$sqlprd);
										$rowprd = mysqli_fetch_array($resultprd);
										$exp_variant1 = $rowprd['variant_1'];										
										$imp_variant1= split(',',$exp_variant1);
										$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit1 = $rowunit['unitname'];
										
										 $sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit2 = $rowunit['unitname'];
										
										//dynamic variant name also
										$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant1[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant1 = $rowvariant['name'];

										 $sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant2[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant2 = $rowvariant['name']; 

										$dimentionDetails = "";
										if($commonObjctype!='1'){
											if($imp_variant1[0]!="") {
											$dimentionDetails = " - ".$variant_variant1.": " .  $imp_variant1[0] . " " . $variant_unit1;
											}
											if($imp_variant2[0]!="") {
												$dimentionDetails .= " - ".$variant_variant2." : " .  $imp_variant2[0] . " " . $variant_unit2;
											}
										}else{
											if($variant_unit2!="") {
												//$dimentionDetails = " - Pcs : " . $variant_unit2;
												$dimentionDetails .= " - ".$variant_variant2." : " . $variant_unit2;
											}
										}
										
										
										
										
										echo '<tr class="odd gradeX">';
										echo '<td>'.$row["brandnm"].'</td>
										<td>'.$row['categorynm'].'</td>
										<td><a href="Order1.php?recieved='.$recieved.'&id='.$row['product_varient_id'].'">'. $obj_product->productname . $dimentionDetails .'</a></td>
										<td align="right">'.$free_quantity.'</td>
										<td align="right">'.$sale_quantity.'</td>
										<td align="right">'.$row['quantity'].'</td>
										</tr>';
									}
								break;
								
								case "Superstockist":									
									//$sql = "SELECT *,SUM(OV.variantunit) as quantity FROM `tbl_order_app` AS OA LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id WHERE `status`!='3' AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' GROUP BY OV.productid";
									$sql = "SELECT *,SUM(OV.variantunit) as quantity FROM `tbl_order_app` AS OA LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id WHERE OV.status='2' AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' GROUP BY OV.product_varient_id";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										/*$sql_brand_category = "SELECT categorynm, (SELECT name FROM tbl_brand WHERE id = brandid) as brand
										FROM `tbl_category` WHERE tbl_category.id = ".$row['catid'];
										$brand_category = mysqli_query($con,$sql_brand_category);
										$obj_brand_cat = mysqli_fetch_object($brand_category);
										*/
										$sql_product = "SELECT  productname FROM `tbl_product` WHERE id = ".$row['productid'];
										$product_result = mysqli_query($con,$sql_product);
										$obj_product = mysqli_fetch_object($product_result);
										
										//$sql_free_pro = "SELECT SUM(variantunit) as quantity_free FROM `tbl_variant_order` INNER JOIN `tbl_order_app` ON tbl_order_app.id = tbl_variant_order.orderappid WHERE campaign_sale_type='free' AND `status`!='3' AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' AND productid = ".$row['productid'];
										$sql_free_pro = "SELECT SUM(variantunit) as quantity_free FROM `tbl_variant_order` AS OV INNER JOIN `tbl_order_app` ON tbl_order_app.id = OV.orderappid WHERE campaign_sale_type='free' AND OV.status='2' AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' AND product_varient_id = ".$row['product_varient_id'];
										$result_free_pro = mysqli_query($con,$sql_free_pro);
										$row_free_pro = mysqli_fetch_assoc($result_free_pro);
										$free_quantity = 0;
										$sale_quantity = $row['quantity'];
										if($row_free_pro['quantity_free'] > 0)
										{
											$free_quantity = $row_free_pro['quantity_free'];
											$sale_quantity = $row['quantity'] - $free_quantity;
										}
										
										$product_varient_id = $row['product_varient_id'];
										$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
										$resultprd = mysqli_query($con,$sqlprd);
										$rowprd = mysqli_fetch_array($resultprd);
										$exp_variant1 = $rowprd['variant_1'];
										$imp_variant1= split(',',$exp_variant1);
										$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit1 = $rowunit['unitname'];
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit2 = $rowunit['unitname'];
										
										
										
										//dynamic variant name also
										$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant1[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant1 = $rowvariant['name'];
										
										$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant2[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant2 = $rowvariant['name']; 
										
										$dimentionDetails = "";
										if($commonObjctype!='1'){
											if($imp_variant1[0]!="") {
											$dimentionDetails = " - ".$variant_variant1.": " .  $imp_variant1[0] . " " . $variant_unit1;
											}
											if($imp_variant2[0]!="") {
												$dimentionDetails .= " - ".$variant_variant2." : " .  $imp_variant2[0] . " " . $variant_unit2;
											}
										}else{
											if($variant_unit2!="") {
												$dimentionDetails = " - ".$variant_variant2." : " . $variant_unit2;
											}
										}
										
										
										
										echo '<tr class="odd gradeX">';										
										echo '<td>'.$row["brandnm"].'</td>
										<td>'.$row['categorynm'].'</td>
										<td><a href="Order1.php?id='.$row['product_varient_id'].'">'.$obj_product->productname .$dimentionDetails .'</a></td>
										<td>'.$free_quantity.'</td>
										<td>'.$sale_quantity.'</td>
										<td>'.$row['quantity'].'</td>
										</tr>';
									}
								break;
								case "Distributor":									
									$sql = "SELECT *,SUM(OV.variantunit) as quantity FROM `tbl_order_app` AS OA LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id WHERE OV.status = 1 AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' GROUP BY OV.product_varient_id";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										/*$sql_brand_category = "SELECT categorynm, (SELECT name FROM tbl_brand WHERE id = brandid) as brand
										FROM `tbl_category` WHERE tbl_category.id = ".$row['catid'];
										$brand_category = mysqli_query($con,$sql_brand_category);
										$obj_brand_cat = mysqli_fetch_object($brand_category);
										*/
										
										$sql_product = "SELECT  productname FROM `tbl_product` WHERE id = ".$row['productid'];
										$product_result = mysqli_query($con,$sql_product);
										$obj_product = mysqli_fetch_object($product_result);
										
										$sql_free_pro = "SELECT SUM(variantunit) as quantity_free FROM `tbl_variant_order` AS OV INNER JOIN `tbl_order_app` ON tbl_order_app.id = OV.orderappid WHERE OV.status = 1 AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' AND campaign_sale_type='free' AND product_varient_id = ".$row['product_varient_id'];
										
										$result_free_pro = mysqli_query($con,$sql_free_pro);
										$row_free_pro = mysqli_fetch_assoc($result_free_pro);
										$free_quantity = 0;
										$sale_quantity = $row['quantity'];
										if($row_free_pro['quantity_free'] > 0)
										{
											$free_quantity = $row_free_pro['quantity_free'];
											$sale_quantity = $row['quantity'] - $free_quantity;
										}
										
										$product_varient_id = $row['product_varient_id'];
										 $sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
										$resultprd = mysqli_query($con,$sqlprd);
										$rowprd = mysqli_fetch_array($resultprd);
										$exp_variant1 = $rowprd['variant_1'];
										$imp_variant1= split(',',$exp_variant1);
										$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit1 = $rowunit['unitname'];
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit2 = $rowunit['unitname'];
										
										//dynamic variant name also
										 $sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant1[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant1 = $rowvariant['name'];
										
										$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant2[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant2 = $rowvariant['name']; 
										
										$dimentionDetails = "";
										if($commonObjctype!='1'){
											if($imp_variant1[0]!="") {
											$dimentionDetails = " - ".$variant_variant1.": " .  $imp_variant1[0] . " " . $variant_unit1;
											}
											if($imp_variant2[0]!="") {
												$dimentionDetails .= " - ".$variant_variant2." : " .  $imp_variant2[0] . " " . $variant_unit2;
											}
										}else{
											if($variant_unit2!="") {
												$dimentionDetails = " - ".$variant_variant2." : " . $variant_unit2;
											}
										}
										
										echo '<tr class="odd gradeX">        
										<td>'.$row["brandnm"].'</td>
										<td>'.$row['categorynm'].'</td>
										
										<td><a href="Order1.php?id='.$row['product_varient_id'].'">'.$obj_product->productname.$dimentionDetails .'</a></td>
										<td>'.$free_quantity.'</td>
										<td>'.$sale_quantity.'</td>
										<td>'.$row['quantity'].'</td>
										</tr>';
									}
								break;
							} 
							?>	
							
						</tbody>
						</table>

						</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->

<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">Order Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script type="text/javascript">
$(document).ready(function(){
	$('#orderidall').bind('click', function(){
  		$('.orderid').prop('checked', this.checked);
});
})

function confirm_place_order(){
	  var order_selected = false;
  $('.orderid').each(function(){
  	if($(this).is(':checked'))
  		order_selected = true;
  })
  if(!order_selected)
  {
  	alert('Please select order to place.');
  	return false;
  }
	var con = confirm("Are you sure, You want to place order?");
	if(con)
	{
		document.forms.place_order_form.submit();
	}
}
function get_order_info(id) {
  console.log('id',id)

  $.ajax
  ({
    type: "POST",
    url: "ajax_orderinfo.php",
   data: "action=get_order&id="+id,
    success: function(msg)
    {
      $("#ajax_list_div").html(msg);
      $('.modal-backdrop').show();

      $('#basicModal').show();

    }
  });
}
function close_modal(){
	$('.modal-backdrop').hide();
	$('#basicModal').hide();
}
</script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>