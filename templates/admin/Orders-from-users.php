<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/commonManage.php";	
$commonObj 	= 	new commonManage($con,$conmain);
$commonObjctype 	= 	$commonObj->log_get_commonclienttype($con,$conmain);

function weekDayToTime($week, $year, $dayOfWeek = 0) {
	$dayOfWeekRef = date("w", mktime (0,0,0,1,4,$year));
	if ($dayOfWeekRef == 0) $dayOfWeekRef = 7;
	$resultTime = mktime(0,0,0,1,4,$year) + ((($week - 1) * 7 + ($dayOfWeek - $dayOfWeekRef)) * 86400);
	$resultTime = cleanTime($resultTime);  //Cleaning daylight saving time hours
	return $resultTime;
};

function cleanTime($time) {
	//This function strips all hours, minutes and seconds from time.
	//For example useful of cleaning up DST hours from time
	$cleanTime = mktime(0,0,0,date("m", $time),date("d", $time),date("Y", $time));
	return $cleanTime;
}
function weeks($year)
{   
	return date("W",mktime(0,0,0,12,28,$year));
}
?>
<!-- END HEADER -->
<?php
$recieved = $_GET["recieved"];
if(isset($_POST['action']) && $_POST['action'] == 'place_order')
{
	$user_id = $_SESSION[SESSION_PREFIX.'user_id'];	
	foreach ($_POST['orderid'] as $key => $orderid) {
		$sql_update = mysqli_query($con,"UPDATE `tbl_order_app` SET status = '3' WHERE superstockistid = '$user_id' and id = '$orderid' ");
	}
	header("location:Orders.php");
	//var_dump($_POST); exit;
} ?>

<style>
.form-horizontal .control-label {
    text-align: left;
}
</style>
<script>
function fnSelectionBoxTest()
{
	if(document.getElementById('selTest').value == '3')
	{
	   document.getElementById('date-show').style.display = "block";
	   document.getElementById('dvTestData').style.display = "none"; 
	}
	else if(document.getElementById('selTest').value == '2')
	{
	   document.getElementById('dvTest').style.display = "none";
	   document.getElementById('dvTestData').style.display = "block"; 
	}
	else
	{
	   document.getElementById('dvTest').style.display = "none";
	   document.getElementById('dvTestData').style.display = "none";
	}
}
</script>
</head>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Orders"; $activeMenu = "Orders";
	
	if($recieved=="admin") {
		$activeMenu = "OrderVisibility";
	}
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<h3 class="page-title">Orders</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Orders</a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Search Criteria</div>
						</div>
						<div class="portlet-body">					
						
						<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
							
							
							<div class="form-group" id="divDaily" >
								<label class="col-md-3">Select date:</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="<?php echo date('d-m-Y');?>">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->
							
						
							<div class="form-group">
								<label class="col-md-3">Stockist:</label>
								<div class="col-md-4" id="divStocklistDropdown">
								 <select name="dropdownStockist" id="dropdownStockist" class="form-control" onchange="fnShowSalesperson(this)">
									<option value="">-Select-</option>
									<?php
									$user_type="Distributor";
									switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":
											$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' order by firstname";									
										break;
										case "Superstockist":
											$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' AND external_id='".$_SESSION[SESSION_PREFIX."user_id"]."' order by firstname";
										break;
										case "Distributor":
											//$condn=" AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
										break;
									}
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										$assign_id=$row['id'];
										echo "<option value='$assign_id' >" . fnStringToHTML($row['firstname']) . "</option>";
									}
									?>
									</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Sales Person:</label>
								<div class="col-md-4" id="divsalespersonDropdown">
									<select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
										<option value="">-Select-</option>
										<?php
										$user_type="SalesPerson";
										switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":
												$sql="SELECT DISTINCT(u.firstname),u.id FROM `tbl_user` as u INNER JOIN tbl_order_app as oa ON oa.order_by= u.id where u.user_type ='$user_type' order by firstname";									
											break;
											case "Superstockist":
											
											$sql="SELECT DISTINCT(u.firstname),u.id FROM `tbl_user` as u INNER JOIN tbl_order_app as oa ON oa.order_by= u.id where u.user_type ='$user_type' AND oa.superstockistid='".$_SESSION[SESSION_PREFIX."user_id"]."' order by firstname";
											break;
											case "Distributor":
												
												$sql="SELECT DISTINCT(u.firstname),u.id FROM `tbl_user` as u INNER JOIN tbl_order_app as oa ON oa.order_by= u.id where u.user_type ='$user_type' AND oa.distributorid='".$_SESSION[SESSION_PREFIX."user_id"]."' order by firstname";
											break;
										} 
										//echo $sql;
										$result1 = mysqli_query($con,$sql);
										while($row = mysqli_fetch_array($result1))
										{
											$assign_id=$row['id'];
											echo "<option value='$assign_id'>" . fnStringToHTML($row['firstname']) . "</option>";
										}?>
									</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group" >
								<label class="col-md-3">State:</label>
								<div class="col-md-4">
								<select name="dropdownState" id="dropdownState" class="form-control" onChange="fnShowCity(this.value)">
									<option value="">-Select-</option>
									<?php
									$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
									$result = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result))
									{
										$cat_id=$row['id'];
										echo "<option value='$cat_id'>" . $row['name'] . "</option>";
									} ?>
									</select>
								</div>
							</div>	
							
							<div class="form-group" id="city_div" >
							  <label class="col-md-3">City:</label>
							  <div class="col-md-4" id="div_select_city">
							  <select name="dropdownCity" id="dropdownCity" data-parsley-trigger="change" class="form-control">
								<option selected value="">-Select-</option>										
								</select>
							  </div>
							</div><!-- /.form-group -->
							
							<div class="form-group" id="area_div" >
							  <label class="col-md-3">Area:</label>
							  <div class="col-md-4" id="div_select_area">
							  <select name="dropdownSuburbs" id="dropdownSuburbs"  class="form-control">
								<option selected value="">-Select-</option>									
								</select>
							  </div>
							</div><!-- /.form-group --> 
							
							<div class="form-group" >
								<label class="col-md-3">Shops:</label>
								<div class="col-md-4" id="divShopdropdown">
								 <select name="dropdownshops" id="dropdownshops" class="form-control">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT name , id FROM tbl_shops ORDER BY name";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . fnStringToHTML($row["name"]) . "</option>";
									} ?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Brand:</label>
								<div class="col-md-4">
								 <select name="dropdownbrands" id="dropdownbrands" class="form-control" onchange="fnShowCategories(this)">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT name , id FROM tbl_brand order by name";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . fnStringToHTML($row["name"]) . "</option>";
									} ?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Category:</label>
								<div class="col-md-4" id="divCategoryDropDown">
								 <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT categorynm , id FROM tbl_category order by categorynm";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . fnStringToHTML($row["categorynm"]) . "</option>";
									}
									?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Product:</label>
								<div class="col-md-4" id="divProductdropdown">
								 <select name="dropdownProducts" id="dropdownProducts" class="form-control">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT productname , id ,catid  FROM tbl_product order by productname";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
									$prodid=$row["id"];
										$catid=$row["catid"];
										$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE productid = '$prodid' ";
										$resultprd = mysqli_query($con,$sqlprd);
										$rowprd = mysqli_fetch_array($resultprd);
										$exp_variant1 = $rowprd['variant_1'];
										$imp_variant1= explode(',',$exp_variant1);
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit1 = $rowunit['unitname'];
										
										$sql="SELECT categorynm , id FROM `tbl_category` WHERE id='$catid'";
										$resultcat = mysqli_query($con,$sql);
										$rowcat = mysqli_fetch_array($resultcat);
										$catname = $rowcat['categorynm'];
									echo "<option value='".$row["id"]."'>" .$catname."-". fnStringToHTML($row["productname"]) ."-".$variant_unit1. "</option>";
									}
									?>
								</select>
								</div>
							</div><!-- /.form-group -->

							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>
									
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType('daily');">Reset</button>
								</div>
							</div><!-- /.form-group -->
						
						</form>							
						
						</div>
					   <div class="clearfix"></div>
					</div>
					
                          
					 <div class="clearfix"></div>   
					<form class="form-horizontal" method="post" name="place_order_form">
						<input type="hidden" name="action" value="place_order">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Received From Users
							</div>
                          
						</div>
						<div class="portlet-body">
			
						<table class="table table-striped table-bordered table-hover" id="sample_2">
						<thead>
							<tr>
							<th width="20%">Brand</th>
							<th width="20%">Brand</th>
							<th width="25%">Category</th>
							<th width="25%">Product</th>
							<th width="10%">Free Quantity</th>
							<th width="10%">Sale Quantity</th>
							<th width="10%">Total Quantity</th>
							</tr>
						</thead>
						<tbody>	
							<?php
							switch($_SESSION[SESSION_PREFIX.'user_type']) {
								
								case "Admin":
								
									$statusCondition = "";
									$statusCondition_inside = "";
									if($recieved!="admin") {
										$statusCondition = " WHERE OV.status='3' ";
										$statusCondition_inside = " AND OV.status='3' ";
									}
								
									$sql = "SELECT *,SUM(OV.variantunit) as quantity FROM `tbl_order_app` AS OA LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id $statusCondition GROUP BY OV.product_varient_id";
									
									$result1 = mysqli_query($con,$sql);
									
									while($row = mysqli_fetch_array($result1)) {
										
										/*$sql_brand_category = "SELECT categorynm, (SELECT name FROM tbl_brand WHERE id = brandid) as brand FROM `tbl_category` WHERE tbl_category.id = ".$row['catid'];
										
										$brand_category = mysqli_query($con,$sql_brand_category);
										$obj_brand_cat = mysqli_fetch_object($brand_category);
										*/
										
										$sql_product = "SELECT  productname FROM `tbl_product` WHERE id = '".$row['productid'] ."'";
										
										$product_result = mysqli_query($con,$sql_product);
										$obj_product = mysqli_fetch_object($product_result);
										
										$sql_free_pro = "SELECT SUM(variantunit) as quantity_free FROM `tbl_variant_order` AS OV INNER JOIN `tbl_order_app` ON tbl_order_app.id = OV.orderappid WHERE campaign_sale_type='free' AND product_varient_id = '".$row['product_varient_id'] ."' $statusCondition_inside";
										
										$result_free_pro = mysqli_query($con,$sql_free_pro);
										$row_free_pro = mysqli_fetch_assoc($result_free_pro);
										$free_quantity = 0;
										$sale_quantity = $row['quantity'];
										if($row_free_pro['quantity_free'] > 0)
										{
											$free_quantity = $row_free_pro['quantity_free'];
											$sale_quantity = $row['quantity'] - $free_quantity;
										}
										
										$product_varient_id = $row['product_varient_id'];
										$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
										$resultprd = mysqli_query($con,$sqlprd);
										$rowprd = mysqli_fetch_array($resultprd);
										$exp_variant1 = $rowprd['variant_1'];										
										$imp_variant1= split(',',$exp_variant1);
										$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit1 = $rowunit['unitname'];
										
										 $sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit2 = $rowunit['unitname'];
										
										//dynamic variant name also
										$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant1[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant1 = $rowvariant['name'];

										 $sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant2[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant2 = $rowvariant['name']; 

										$dimentionDetails = "";
										if($commonObjctype!='1'){
											if($imp_variant1[0]!="") {
											$dimentionDetails = " - ".$variant_variant1.": " .  $imp_variant1[0] . " " . $variant_unit1;
											}
											if($imp_variant2[0]!="") {
												$dimentionDetails .= " - ".$variant_variant2." : " .  $imp_variant2[0] . " " . $variant_unit2;
											}
										}else{
											if($variant_unit2!="") {
												//$dimentionDetails = " - Pcs : " . $variant_unit2;
												$dimentionDetails .= " - ".$variant_variant2." : " . $variant_unit2;
											}
										}
										echo '<tr class="odd gradeX">
										<td>'.$row["brandnm"].'</td>
										<td>'.$row["brandnm"].'</td>
										<td>'.$row['categorynm'].'</td>
										<td><a href="Order1.php?recieved='.$recieved.'&id='.$row['product_varient_id'].'">'. $obj_product->productname . $dimentionDetails .'</a></td>
										<td align="right">'.$free_quantity.'</td>
										<td align="right">'.$sale_quantity.'</td>
										<td align="right">'.$row['quantity'].'</td>
										</tr>';
									}
								break;
								
								
								case "Distributor":									
									$sql = "SELECT *,SUM(OV.variantunit) as quantity FROM `tbl_order_app` AS OA LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id WHERE OV.status = 1 AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' GROUP BY OV.product_varient_id";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										/*$sql_brand_category = "SELECT categorynm, (SELECT name FROM tbl_brand WHERE id = brandid) as brand
										FROM `tbl_category` WHERE tbl_category.id = ".$row['catid'];
										$brand_category = mysqli_query($con,$sql_brand_category);
										$obj_brand_cat = mysqli_fetch_object($brand_category);
										*/
										
										$sql_product = "SELECT  productname FROM `tbl_product` WHERE id = ".$row['productid'];
										$product_result = mysqli_query($con,$sql_product);
										$obj_product = mysqli_fetch_object($product_result);
										
										$sql_free_pro = "SELECT SUM(variantunit) as quantity_free FROM `tbl_variant_order` AS OV INNER JOIN `tbl_order_app` ON tbl_order_app.id = OV.orderappid WHERE OV.status = 1 AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' AND campaign_sale_type='free' AND product_varient_id = ".$row['product_varient_id'];
										
										$result_free_pro = mysqli_query($con,$sql_free_pro);
										$row_free_pro = mysqli_fetch_assoc($result_free_pro);
										$free_quantity = 0;
										$sale_quantity = $row['quantity'];
										if($row_free_pro['quantity_free'] > 0)
										{
											$free_quantity = $row_free_pro['quantity_free'];
											$sale_quantity = $row['quantity'] - $free_quantity;
										}
										
										$product_varient_id = $row['product_varient_id'];
										 $sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
										$resultprd = mysqli_query($con,$sqlprd);
										$rowprd = mysqli_fetch_array($resultprd);
										$exp_variant1 = $rowprd['variant_1'];
										$imp_variant1= split(',',$exp_variant1);
										$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit1 = $rowunit['unitname'];
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit2 = $rowunit['unitname'];
										
										//dynamic variant name also
										 $sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant1[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant1 = $rowvariant['name'];
										
										$sql="SELECT TV.id,TV.name FROM `tbl_units_variant` TUV
											left join tbl_variant TV on TUV.variantid=TV.id WHERE TUV.unitname='$imp_variant2[1]'";
										$resultvariant = mysqli_query($con,$sql);
										$rowvariant = mysqli_fetch_array($resultvariant);
										$variant_variant2 = $rowvariant['name']; 
										
										$dimentionDetails = "";
										if($commonObjctype!='1'){
											if($imp_variant1[0]!="") {
											$dimentionDetails = " - ".$variant_variant1.": " .  $imp_variant1[0] . " " . $variant_unit1;
											}
											if($imp_variant2[0]!="") {
												$dimentionDetails .= " - ".$variant_variant2." : " .  $imp_variant2[0] . " " . $variant_unit2;
											}
										}else{
											if($variant_unit2!="") {
												$dimentionDetails = " - ".$variant_variant2." : " . $variant_unit2;
											}
										}
										
										echo '<tr class="odd gradeX">
											<td>'.$row["brandnm"].'</td>
											<td>'.$row["brandnm"].'</td>
										<td>'.$row['categorynm'].'</td>
										
										<td><a href="Order1.php?id='.$row['product_varient_id'].'">'.$obj_product->productname.$dimentionDetails .'</a></td>
										<td>'.$free_quantity.'</td>
										<td>'.$sale_quantity.'</td>
										<td>'.$row['quantity'].'</td>
										</tr>';
									}
								break;
							} 
							?>	
							
						</tbody>
						</table>

						</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->

<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">Order Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script type="text/javascript">

$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});

$(document).ready(function(){
	$('#orderidall').bind('click', function(){
  		$('.orderid').prop('checked', this.checked);
});
})

function confirm_place_order(){
	  var order_selected = false;
  $('.orderid').each(function(){
  	if($(this).is(':checked'))
  		order_selected = true;
  })
  if(!order_selected)
  {
  	alert('Please select order to place.');
  	return false;
  }
	var con = confirm("Are you sure, You want to place order?");
	if(con)
	{
		document.forms.place_order_form.submit();
	}
}
function get_order_info(id) {
  console.log('id',id)

  $.ajax
  ({
    type: "POST",
    url: "ajax_orderinfo.php",
   data: "action=get_order&id="+id,
    success: function(msg)
    {
      $("#ajax_list_div").html(msg);
      $('.modal-backdrop').show();

      $('#basicModal').show();

    }
  });
}
function close_modal(){
	$('.modal-backdrop').hide();
	$('#basicModal').hide();
}
</script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>