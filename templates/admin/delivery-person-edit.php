<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);

?>
<!-- END HEADER -->
<?php

if(isset($_POST['hidbtnsubmit']))
{
	$id=$_POST['id'];
	$user_type="DeliveryPerson";	
	$userObj->updateLocalUserDetails($user_type, $id);
	
	$working_detail = $userObj->getLocalUserWorkingAreaDetails($id);
	if($working_detail == 0)
		$userObj->addLocalUserWorkingAreaDetails($id);
	else
		$userObj->updateLocalUserWorkingAreaDetails($id);	
	
	$userObj->updateCommonUserDetails($id);
	
	echo '<script>alert("Delivery Person updated successfully.");location.href="delivery-persons.php";</script>';
	
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "DeliveryPerson";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<h3 class="page-title">Delivery Person</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="delivery-persons.php">Delivery Person</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#"><? if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") { echo "Delivery Person Details"; } else { echo "Edit Delivery Person"; } ?></a> 
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<? if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") { echo "Delivery Person Details"; } else { echo "Edit Delivery Person"; } ?>
							</div>
							 <a href="updatepassword.php?id=<?php echo base64_encode($_GET['id']);?>" class="btn btn-sm btn-default pull-right mt5">							
                                Change Password
                              </a>
						</div>
						<div class="portlet-body">
						<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						<? } ?>
					    <?php
						$id			= $_GET['id'];
						$userObj 	= 	new userManager($con,$conmain);
						$user_details = $userObj->getLocalUserDetails($id);
						$user_bank_details = $userObj->getLocalUserBankDetails($id);
						$working_area_details = $userObj->getLocalUserWorkingAreaDetails($id);
						if($working_area_details != 0)
						{
							$row1 = array_merge($user_details,$working_area_details);//print"<pre>";print_r($row1);
							if($row1['state_ids'] == '')
								$row1['state_ids'] = $user_details['state'];
							if($row1['city_ids'] == '')
								$row1['city_ids'] = $user_details['city'];
						}
						else{
							$row1 = $user_details;
							$row1['state_ids'] = $user_details['state'];
							$row1['city_ids'] = $user_details['city'];
						}											
					
						$parentid	= $user_details['sstockist_id'];
						?>                       

					<form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" action="" method="post">
						
						<?php $page_to_update = 'delivery_person'; include "userUpdateCommEle.php";	//form common element file with javascript validation ?>    
						<div class="form-group">
						  <label class="col-md-3">Office Phone No.:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Office Phone No."
							data-parsley-trigger="change"					
							data-parsley-minlength="10"
							data-parsley-maxlength="15"
							data-parsley-maxlength-message="Only 15 characters are allowed"
							data-parsley-pattern="^(?!\s)[0-9]*$"
							data-parsley-pattern-message="Please enter numbers only"
							name="officephone" value="<?php if($user_bank_details['officephone'])echo $user_bank_details['officephone'];else{echo '-';}?>" 
							class="form-control">
						  </div>
						</div>
						<div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name <span class="mandatory">*</span></label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"				
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter accouont number"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="accholdername" 
								value="<?php if($user_bank_details['accholdername'])echo $user_bank_details['accholdername'];else{echo '-';}?>" 
								class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter accouont number"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							<?php if($_SESSION[SESSION_PREFIX."user_type"]=="Distributor") { echo "disabled"; }?>
							name="accno" class="form-control" value="<?php if($user_bank_details['accno'])echo $user_bank_details['accno'];else{echo '-';}?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter branch Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							<?php if($_SESSION[SESSION_PREFIX."user_type"]=="Distributor") { echo "disabled"; }?>
							name="accbrnm" class="form-control" value="<?php if($user_bank_details['accbrnm'])echo $user_bank_details['accbrnm'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter ifsc Code"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							<?php if($_SESSION[SESSION_PREFIX."user_type"]=="Distributor") { echo "disabled"; }?>
							name="accifsc" class="form-control" value="<?php if($user_bank_details['accifsc'])echo $user_bank_details['accifsc'];?>">
						  </div>
						</div>
						
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
								<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
								<input type="hidden" name="hidAction" id="hidAction" value="delivery-person-edit.php">
								<input type="hidden" name="id" id="id" value="<?=$row1['id'];?>">
								<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
								<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
								<? } ?>
								<a href="delivery-persons.php" class="btn btn-primary">Cancel</a>
							</div>
						</div><!-- /.form-group -->
					</form>  
				   <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
					<div class="modal-dialog" style="width:300px;">
						<div class="modal-content">
							<div class="modal-body">
								<p>
								<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
								</p>                     
							  <center><a href="sales_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
							  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
							  </center>
							</div>    
						</div>
					</div>
					</div>  
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<!-- END PAGE LEVEL SCRIPTS -->
<script>
function fnShowStockist(id){
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetchstockist.php?cat_id="+id.value+"&multiple=multiple&user_type_manage=sales",true);
xmlhttp.send();
 }
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>