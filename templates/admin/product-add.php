<?php
 include "../includes/header.php";
 include "../includes/commonManage.php";	
$sqlvariant1="SELECT name FROM tbl_variant WHERE id='40'";
$resultprd1 = mysqli_query($con,$sqlvariant1);
$rowvariant1 = mysqli_fetch_array($resultprd1);
//////////////////////////////////////////////////////////////////////
$sqlvariant2="SELECT name FROM tbl_variant WHERE id='41'";
$resultprd2 = mysqli_query($con,$sqlvariant2);
$rowvariant2 = mysqli_fetch_array($resultprd2);
/////////////////////////////////////////////////////////////////////
$commonObj 	= 	new commonManage($con,$conmain);
$commonObjctype 	= 	$commonObj->log_get_commonclienttype($con,$conmain);

if(isset($_POST['submit']))
{
	//exit;
	//print_r($_POST);exit;
	$price=$_POST['price'];
	$variant1=$_POST['variant1'];
	$variant2=$_POST['variant2'];
	$countobj=count($price);
	
	$catid=$_POST['catid'];
	$productname=fnEncodeString($_POST['productname']);
	
	$sql_product_check=mysqli_query($con,"select id from `tbl_product` where productname='$productname'");
	
	if($rowcount = mysqli_num_rows($sql_product_check)>0){	
		echo '<script>alert("Duplicate Product not added.");location.href="product-add.php";</script>';
	}else{
		 $sqlprod="INSERT INTO `tbl_product` (catid,productname) VALUES('$catid','$productname')";
		$sql_product_add=mysqli_query($con,$sqlprod);	
		$sql_id= mysqli_insert_id($con);
		
		
		//echo "<pre>";print_r($commonObjctype);
		
		$commonObj->log_add_record('tbl_product',$sql_id,$sqlprod);	
		for($i=0;$i<$countobj;$i++){
				
				$file=$_FILES['productimage']['name'][$i];
				//echo"dsfsd123   "; var_dump($file); echo "   dsfsd";
				$folder = "upload/";
				$upload_image = $folder . basename($_FILES["productimage"]["name"][$i]);
				$str=move_uploaded_file($_FILES["productimage"]["tmp_name"][$i], $upload_image);
				
				$barcode_file=$_FILES['productbarcodeimage']['name'][$i];
				$upload_barcode_image = $folder . basename($_FILES["productbarcodeimage"]["name"][$i]);
				$str_barcode=move_uploaded_file($_FILES["productbarcodeimage"]["tmp_name"][$i], $upload_barcode_image);
				
				$varcnt=$i+1;
				$var1= $variant1[$i*2].','.$variant1[($i*2)+1];
				$var2= $variant2[$i*2].','.$variant2[($i*2)+1];
				$pri1=$price[$i];
				$p_hsn = $_POST['producthsn'][$i];
				$cgst = $_POST['cgst'][$i];
				$sgst = $_POST['sgst'][$i];
				$mrpprice = $_POST['mrpprice'][$i];
				$pricetostockist = $_POST['pricetostockist'][$i];
				$pricetoretailer = $_POST['pricetoretailer'][$i];
				
				 $sqlvar="INSERT INTO `tbl_product_variant`(productid,variant_1,variant_2,price,mrpprice,variant_cnt,productimage,
				 productbarcodeimage,producthsn,
				 cgst,sgst,pricetostockist,pricetoretailer) 
				 VALUES ('$sql_id','$var1','$var2','$pri1','$mrpprice','$varcnt','$file','$barcode_file','$p_hsn','$cgst','$sgst'
				 ,'$pricetostockist','$pricetoretailer')";
				//"INSERT INTO `tbl_product_variant`(productid,variant_1,variant_2,price,variant_cnt) VALUES ('$sql_id','$var1','$var2','$pri1',1)";
				$sql3=mysqli_query($con,$sqlvar);
				$sql_variant_id= mysqli_insert_id($con);
				$commonObj 	= 	new commonManage($con,$conmain);
				$commonObj->log_add_record('tbl_product_variant',$sql_variant_id,$sqlvar);	
		}
		echo '<script>alert("Product added successfully.");location.href="product.php";</script>';
	}
	
	
	exit;
	
} ?>
<!-- BEGIN HEADER -->
 
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Product";
	include "../includes/sidebar.php";
	?>
	
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Product
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="product.php">Product</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Product</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Product
							</div>
							
						</div>
						<div class="portlet-body">
                        <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                        <form id="productaddform" class="form-horizontal" data-parsley-validate="" role="form" method="post" action="product-add.php" enctype="multipart/form-data">
          
            
            <div class="form-group">
              <label class="col-md-3">Category:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <select name="catid"
              data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select category"
				class="form-control">
                <option  selected disabled>-Select-</option>
				<?php
				$sql="SELECT c.id as id,b.name, c.categorynm FROM `tbl_category` c,tbl_brand b WHERE b.id=c.brandid";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
				$id=$row['id'];
				echo "<option value='$id'>".fnStringToHTML($row['name'])."-" . fnStringToHTML($row['categorynm']) . "</option>";
				}
				?>
                </select>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Product Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" name="productname"
                placeholder="Enter Product Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter product name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
		<div id="prodvar">
            <div class="form-group" 
			<?php if($commonObjctype=='1'){
				echo 'style="display: none;"';
			} ?>>
              <label class="col-md-3"><?php echo fnStringToHTML($rowvariant1['name']);?>:</label>
		          <div class="col-md-4 nopadl">		  

              <div class="col-sm-6">
                <input type="text" name="variant1[]" id="numunits1" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[a-zA-Z0-9-!@#$%^&*+_=><,./:' ]*$" class="form-control">
              </div>
              
              <div class="col-md-6">
                <select name="variant1[]" id="units_variant_id1"  class="form-control">
				<?php 
				$sql_id="SELECT  * FROM `tbl_units_variant` WHERE variantid =40";
				$result_id = mysqli_query($con,$sql_id);
				while($row_id = mysqli_fetch_array($result_id))
				{
					$unitname=$row_id['unitname'];
					$sql="SELECT  unitname,id FROM `tbl_units` WHERE id='$unitname'";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result))
					{
						$unit=$row['id'];
						echo "<option value='$unit'>".fnStringToHTML($row['unitname'])."</option>";
					}
				}
				?>
                </select>
              </div>
            </div><!-- /.form-group -->
		</div>

            <div class="form-group">
              <label class="col-md-3"><?php echo fnStringToHTML($rowvariant2['name']);?>:</label>
	<div class="col-md-4 nopadl">		
              <div class="col-md-6" <?php if($commonObjctype=='1'){
				echo 'style="display: none;"';
			} ?>>
                <input type="text" name="variant2[]" id="numunits2" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[a-zA-Z0-9-!@#$%^&*+_=><,./:' ]*$" class="form-control">
              </div>
              <div class="col-md-6">
                <select name="variant2[]" id="units_variant_id2" class="form-control">
				<?
				$sql_id="SELECT  * FROM `tbl_units_variant` WHERE variantid = 41 ";
				$result_id = mysqli_query($con,$sql_id);
				while($row_id = mysqli_fetch_array($result_id))
				{
					$unitname=$row_id['unitname'];
					$sql="SELECT  unitname,id FROM `tbl_units` WHERE id='$unitname'";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result))
					{
						$unit=$row['id'];
						echo "<option value='$unit'>".fnStringToHTML($row['unitname'])."</option>";
					}
				}
				?>
                </select>
              </div>
            </div><!-- /.form-group -->			          
          </div> 
			 <div class="form-group">
              <label class="col-md-3">Product HSN:</label>
              <div class="col-md-4">
                <input type="text" name="producthsn[]"
                placeholder="Enter Product HSN"
                data-parsley-trigger="change"	
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Price:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" name="price[]" id="price[]" placeholder="Price" onkeyup="myFunction(this)" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter product price"
				data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
              </div>
            </div><!-- /.form-group -->	
			<div class="form-group">
              <label class="col-md-3">MRP Price:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" name="mrpprice[]" id="mrpprice[]" placeholder="MRP Price" onkeyup="myFunction(this)" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter product mrp price"
				data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
              </div>
            </div><!-- /.form-group -->	
			<div class="form-group">
              <label class="col-md-3">Price To Stockist:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" name="pricetostockist[]" id="pricetostockist[]" placeholder="Price To Stockist" onkeyup="myFunction(this)" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter price to stockist"
				data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
              </div>
            </div><!-- /.form-group -->	
			<div class="form-group">
              <label class="col-md-3">Price TO Retailer:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" name="pricetoretailer[]" id="pricetoretailer[]" placeholder="Price To Retailer" onkeyup="myFunction(this)" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter price to retailer"
				data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
              </div>
            </div><!-- /.form-group -->	
			<div class="form-group">
              <label class="col-md-3">CGST (%):</label>
              <div class="col-md-4">
                <input type="text" name="cgst[]" id="cgst[]" placeholder="CGST" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
              </div>
            </div><!-- /.form-group -->	
			<div class="form-group">
              <label class="col-md-3">SGST (%):</label>
              <div class="col-md-4">
                <input type="text" name="sgst[]" id="sgst[]" placeholder="SGST" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
              </div>
            </div><!-- /.form-group -->	
            <div class="form-group">
              <label class="col-md-3">Product Image:</label>

              <div class="col-md-4">
                <input type="file" id="productimage" 
				data-parsley-trigger="change"				
				data-parsley-fileextension='png,jpeg,jpg' 
				data-parsley-max-file-size="1000"  
				name="productimage[]" multiple="multiple">
              </div>
			  <div class='form-group'><label class='col-md-1'>
			  <!--<span class='removespan' id="removespan" name="removespan[]">✖</span></label>-->
			  </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Product Barcode Image:</label>

              <div class="col-md-4">
                <input type="file" id="productbarcodeimage" 
				data-parsley-trigger="change"				
				data-parsley-fileextension='png,jpeg,jpg' 
				data-parsley-max-file-size="1000"  
				name="productbarcodeimage[]" multiple="multiple">
              </div>
            </div><!-- /.form-group -->
			
		</div>
			
			<div id="demo"></div>  
		
           <div class="clearfix"></div> 
		   
            
             
                  
            <hr/>      
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
				<a type="button" class="btn btn-primary" id="btn1">Add More Variant</a>
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="product.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group --> 
          </form>          
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->



<?php include "../includes/grid_footer.php"?>

<script>
/*$('#productaddform').submit(function () {
	var arraylen=$("input[name='price[]']").length;
	alert(arraylen);//return false;
	var totalinput1=new Array();var totalinput2=new array();
	$("input[name='variant1[]']").each(function() {
		if($(this).val()!=''){
			totalinput1.push($(this).val());
		}
	});
	$("input[name='variant2[]']").each(function() {
	   if($(this).val()!=''){
			totalinput2.push($(this).val());
		}
	});
	alert(totalinput1);
	alert("   SEperator   ");
	alert(totalinput2);

	alert("temp2323");
	return false;
	
});*/
function myFunction(varl) {
		  if (varl.value != "") {
                var arrtmp = varl.value.split(".");
                if (arrtmp.length > 1) {
                    var strTmp = arrtmp[1];
                    if (strTmp.length > 2) {
                        varl.value = varl.value.substring(0, varl.value.length - 1);
                    }
                }
            }
	}
/* $('#productaddform').submit(function () {
	
}); */
$(document).ready(function() {
    window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
            // the value contains the file path, so we can pop the extension
			var arrRequirement = requirement.split(',');			
            var fileExtension = value.split('.').pop();
			var result = jQuery.inArray( fileExtension, arrRequirement );
			 
			if(result==-1)
            return false;
			else 
			return true;
        }, 32)
        .addMessage('en', 'fileextension', 'Only png or jpg image are allowed');	
window.Parsley.addValidator('maxFileSize', {
  validateString: function(_value, maxSize, parsleyInstance) {
    var files = parsleyInstance.$element[0].files;
    return files.length != 1  || files[0].size <= maxSize * 1024;
  },
  requirementType: 'integer',
  messages: {
    en: 'Image should not be larger than %s Kb',
  }
});
	
	/* $("input[id^='price['").keyup(function (e) {
            if (this.value != "") {
                var arrtmp = this.value.split(".");
                if (arrtmp.length > 1) {
                    var strTmp = arrtmp[1];
                    if (strTmp.length > 2) {
                        this.value = this.value.substring(0, this.value.length - 1);
                    }
                }
            }
        });  */
});
</script>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>