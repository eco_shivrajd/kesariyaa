<?php
include ("../includes/config.php");
if (isset($_POST['submit'])) {
    $user_name = $_POST['user_name'];	
    $result = mysqli_query($con, "select * from tbl_user where user_type!='SalesPerson' and username ='" . $_POST['user_name'] . "'");
    $count = mysqli_fetch_array($result);
	//echo "<pre>";print_r($count);die();	
	if($count)
	{   
		$to  = $count['email'];
		if($to!="")
		{
        $token = strtoupper(strval(bin2hex(openssl_random_pseudo_bytes(16))));
		$id = $count['id'];
        $result = mysqli_query($con, "update `tbl_user` SET reset_key = '$token' where id = '$id'");
		
		$subject   = "Reset Password";
        $fromMail  = FROMMAILID;
		
		//$to        = "virendra.sonawane@ecotechservices.com";
		$semi_rand = md5(time()); 
		$headers = "";
		$headers .= "From: ".$fromMail."\r\n";
		$headers .= "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-Type: text/html;" . "\r\n";
		$message = "";
		$message .= "Hi, <br/>";
		$message .= "Some one has requested reset password for your account.<br/>";
		$message .= "Please find the reset password link below<br/><br/>";

		$message .= "<a href='".SITEURL."/templates/reset_password.php?key=".$token."&id=".$id."'>Click Here</a> to Login<br/>";
		$message .= "<br/><br/>";
		$message .= "Thanks,<br/>";
		$message .= "Admin.";		
		
			$sent = @mail($to,$subject,$message,$headers);
			$e = '<h5 style="color:red;"><b>Reset password link has been sent successfully to your E-mail account. Please check your E-mail account and click on reset link to generate new password.</b></h5>';
		}else{
			$e = '<h5 style="color:red;"><b>There is no email id exist for this user. Please contact to administrator to get new password.</b></h5>';
		}
	}
	else
	{ 		
		$e = '<h5 style="color:red; text-align:center;">Username does not exists.</h5>';
	}
}
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?=SITETITLE;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="../assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="login.php">
	<img src="<?=LOGINPAGELOGOPATH;?>" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<script>
document.onkeydown = function (evt) {
  var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
  if (keyCode == 13) {
	document.getElementById("submit").click();
  }
  if (keyCode == 27) {
document.getElementById("submit").click();
  } else {
    return true;
  }
};
</script>
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="login-form" method="post" action="" data-parsley-validate="" name="form" id="form">
		<h3 class="form-title">Forgot Password</h3>
		<?php echo $e;?>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span></span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">E-mail</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="text"
			data-parsley-trigger="change" data-parsley-required="#true" 
			data-parsley-required-message="Please enter username"			
			placeholder="Username" name="user_name" value="" />
		</div>
		<div class="form-actions">
		<a href="login.php" id="forget-password" class="forget-password">Return to login</a>
		</br></br>
			<button  type="submit" name="submit" id="submit" class="btn btn-success btn-block uppercase">Submit</button>		
		</div>
		
	</form>
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->
	
	<!-- END FORGOT PASSWORD FORM -->
	<!-- BEGIN REGISTRATION FORM -->

	<!-- END REGISTRATION FORM -->
</div>

<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-ui/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/plugins/jquery-ui/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="../assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="../assets/admin/pages/scripts/login.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.7.2/parsley.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Login.init();
Demo.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>